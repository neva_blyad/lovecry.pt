#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
copying.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use CGI;
#use Cwd;
use File::Basename;
use File::Spec;
use Locale::gettext;
use Try::Tiny;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/lib/";
use lib "$path/liberation/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;

# Global Variables:
#
#  - CGI,
our $cgi         = undef;
our $cgi_seSSion = undef;
our $cgi_auth    = undef;
our $cgi_auth_ok = undef;

#  - Database
our $db_locale   = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    return cgi::init();
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    my $username = shift;

    return db::connect($username,
                       0 # Do not create database if it doesn't exist
                      );
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_select
{
    my $db     = shift;
    my $action = shift;

    $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init  ();
    ($cgi_seSSion, $cgi_auth) = cgi_auth  ($cgi);
                                cgi_deinit($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect($cgi_auth->{'profile'}{'username'});

        if (defined $db)
        {
            #db_create($db);
            #db_insert($db);
            db_select($db);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir $path;

    i18n::init(\$db_locale, $cgi);
}
catch
{
    die "[2]: $_";
};

# HTML
print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};
print ${html::header_get(html::title_get(gettext('Copying'),
                                         html::SITE_NAME))};

print html::INDENT1;

if ($cgi_auth_ok)
{
    print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/">), gettext('Index'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);
}

print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);

if ($cgi_auth_ok)
{
    print q(<span id="menu_delimiter">|</span>);

    print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>);
}

print q(

    </div>
    </div>
    <div id="main">
    <div id="dummy">
    <div id="table">
    <div id="main">
    <div id="left">
    </div>
    <div id="centr640">

    <h1>), gettext('Copying'), q(</h1>

    <h2>), gettext('Mascot'), qq(</h2>

    <p id="centr_padding"><img src="/liberation/img/luna-libre_grey.png"></p>

    <p id="padding"><b>), gettext('Luna butterfly'), q(</b> ), gettext('is a project mascot.'), q(</p>
    <p id="padding">), gettext('whereideasoverlap is designer of original 16x16 icon.'), q(</p>

    <h2>), gettext('Legal Disclaimer'), q(</h2>

    <p>), gettext('The web engine licensed under the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.'), q(</p>

    <p id="centr_padding"><img src="/img/copying.png"></p>
    <p id="centr_padding"><a href="https://www.gnu.org/licenses/agpl-3.0.en.html"><img src="/img/agplv3-with-text-162x68.png"></a></p>

    <p>), gettext('The source code is available at this link:'), q(</p>
    <p><a href="https://gitlab.com/neva_blyad/lovecry.pt/">https://gitlab.com/neva_blyad/lovecry.pt/</a></p>

    <h2>), gettext('Third-party Material'), q(</h2>

    <p>), gettext('See below full list of Non-AGPL components distributed by us and their
    corresponded licenses.'), q(</p>

    <ul>
        <li><p>), gettext('All fonts distributed by us are free.'), q(</p>
        <p>), gettext('Copyright information can be found at these links:'), qq(</p>

        <ul>
            <li><a href="/fonts/copyright-lato.txt">www.lovecry.pt/fonts/copyright-lato.txt</a>
            <li><a href="/fonts/copyright-liberation2.txt">www.lovecry.pt/fonts/copyright-liberation2.txt</a>
            <li><a href="/fonts/copyright-lobster.txt">www.lovecry.pt/fonts/copyright-lobster.txt</a>
        </ul>

        <li>), gettext('Luna batterfly icon:'), qq(
        <ul>
            <li><a href="/liberation/img/copyright-luna.txt">www.lovecry.pt/liberation/img/copyright-luna.txt</a>
        </ul>
    </ul>

    <h2>), gettext('Additional Information'), q(</h2>

    <ul>
        <li>), gettext("This is my personal Web server, it's powered by OpenBSD, Apache, Perl, SQLite, they are all Open Source software."), q(
    </ul>

    <p id="centr_padding">
    <a href="https://www.openbsd.org/"><img src="/img/openbsd-logo.png"></a>
    <a href="https://www.apache.org/"><img src="/img/apache-logo.png"></a>
    </p>

    <p id="centr_padding">
    <a href="https://www.perl.com"><img src="/img/perl-logo.png"></a>
    <a href="https://sqlite.org/"><img src="/img/sqlite-logo.png"></a>
    </p>

    <ul>
        <li>
            <table>
                <tr><td>), gettext('Main URL is'), q(</td><td><a href="https://www.lovecry.pt">https://www.lovecry.pt</a>,</td>
                <tr><td>), gettext('mirror is'), q(</td><td><a href="https://www.lovecri.es">https://www.lovecri.es</a>.</td>
            </table>
        <li>), gettext('This product includes GeoLite data created by MaxMind, available from'), q(
            <a href="https://www.maxmind.com">https://www.maxmind.com</a>.
        <li>), gettext("Hello!"), q(
    </ul>

    <p id="centr_padding">
    <img src="/img/cosmos.png">
    </p>

    </div>
    <div id="right">
    </div>
    </div>
    </div>);

print ${html::footer_get()};
