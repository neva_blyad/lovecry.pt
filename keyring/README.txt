 _                    ____                    _   
| |    _____   _____ / ___|_ __ _   _   _ __ | |_ 
| |   / _ \ \ / / _ \ |   | '__| | | | | '_ \| __|
| |__| (_) \ V /  __/ |___| |  | |_| |_| |_) | |_ 
|_____\___/ \_/ \___|\____|_|   \__, (_) .__/ \__|
                                |___/  |_|        
PGP public key server                                         keyring.lovecry.pt

--------------------------------------------------------------------------------

This public key server provides simple HKP/HKPS lookup and add requests for
everyone's public keys.

The server may be accessed with gpg by using the --keyserver option in
combination with either of the --recv-keys or --send-keys actions.

This server supports not only basic key retreive/update operations, but also
implements search functionality using name or email or their parts. Use --search
argument for this.

Only keys in the LoveCry.pt keyrings will be returned by this server.

You can use the keyring server for the following purposes.

--------------------------------------------------------------------------------
 Send your key
--------------------------------------------------------------------------------

Send your key:

$ gpg --keyserver hkps://keyring.lovecry.pt:443 --send-keys 8D408841BC99C2EF

You can check the result with --recv-keys, but note it can take up to 15 minutes
for your submission to be processed. Your updated key will then be included into
the active keyring in our next keyring push (after pre-moderation). 

Note that there is limit of keys the server may accept from the single IP. Only
two keys per day.

Note, we use secure HKPS protocol instead of plain HKP.

--------------------------------------------------------------------------------
 Search a key
--------------------------------------------------------------------------------

Now you could try to find someone's key by his name or/and email:

$ gpg --keyserver hkps://keyring.lovecry.pt:443 --search '@lovecry.pt'

--------------------------------------------------------------------------------
 Receive a key
--------------------------------------------------------------------------------

Once you know the key's ID, just ask the server for it:

$ gpg --keyserver hkps://keyring.lovecry.pt:443 --recv-keys 8D408841BC99C2EF

--------------------------------------------------------------------------------
 Replace your key
--------------------------------------------------------------------------------

To replace an existing key or remove a key from the LoveCry.pt keyring, file an
request by sending email to keyring[A_HERE]lovecry.pt and please remember to
include something descriptive as well

--------------------------------------------------------------------------------
 Revoke a key
--------------------------------------------------------------------------------

If you have any reason to believe your key has been compromised, or there is any
strong reason for you stop trusting your key, do upload your revocation
certificate right away to the keyserver, and file an request as described above.
We will act as quickly as possible.

--------------------------------------------------------------------------------
 Harden your cypherpunk experience
--------------------------------------------------------------------------------

Invite your friends to share their PGP keys.
Encrypt all the mail you send.

--------------------------------------------------------------------------------
 Source code
--------------------------------------------------------------------------------

The GPL'ed source code is available at this link:
https://gitlab.com/neva_blyad/lovecry.pt/-/tree/master/keyring?ref_type=heads
