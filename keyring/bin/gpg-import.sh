#!/usr/bin/env bash

function use
{
    echo "Usage: $0 <out_suffix>"
    exit 1
}

function gpg_import()
{
    local in_filename="$1"
    local out_suffix="$2"
    local out_filename="keyrings-new/lovecry.pt-$out_suffix.gpg"

    gpg --homedir=gnupg/             \
        --no-default-keyring         \
        --no-permission-warning      \
        --keyring="$out_filename"    \
        --fast-import "$in_filename"
    rm "$in_filename"

    #echo "New PGP key has been imported from $in_filename to $out_filename" | \
    #    mail -s Notification keyring@lovecry.pt # cron sent mails, so I'd commented this line
}

if [ $# -ne 1 ] 
then
    use 
fi

OUT_SUFFIX="$1"

export -f gpg_import
cd /home/neva_blyad/Облако/Разработка/Система/lovecry.pt/keyring/

find keyrings-new/incoming/   -type f \
                            ! -name sequence -and ! -name .gitkeep \
                              -exec bash -c "gpg_import \"\$0\" \"$OUT_SUFFIX\"" {} \;
