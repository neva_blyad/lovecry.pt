=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
cgi.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare CGI package
package cgi;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Base class
use base 'CGI::Session::Auth';

# CPAN modules
use CGI;
use CGI::Session;
#use Cwd;
use File::Basename;
use File::Spec;
use Digest::SHA;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../liberation/lib/";

# Our own modules
use db;

# Constants:
#
#  - CGI
use constant TMP => '/tmp/';

use constant AUTH_USE_SESSION => 0;
use constant AUTH_LOGIN       => 1;
use constant AUTH_LOGOUT      => 2;

use constant COOKIE_EXPIRATION => '+3M';

################################################################################
# CGI
################################################################################

sub init
{
    return new CGI();
}

################################################################################

sub space_get
{
    my @url;
    my $space;

    @url   = split /\//, $ENV{'REQUEST_URI'};
    $space = $url[1];

    return $space;
}

################################################################################

sub _login
{
    my $res;
    my $passphrase;
    my $passphrase_real;

    my $self     = shift;

    my $username = shift;
    my $password = shift;

    $self->_debug("username: $username, password: $password");

    # Check password.
    # Check PGP passphrase in case of two-level authorization.
    # Load the user profile.
    $passphrase      = $self->{'passphrase'};
    $passphrase_real = $self->_session->param('passphrase');

    if (defined $self->{'password'} &&
                $self->{'password'} eq Digest::SHA::sha256_hex($password . db::PASSWORD_SALT) &&

        ((!defined $passphrase_real && !defined $passphrase) ||
          (defined $passphrase_real &&  defined $passphrase && $passphrase_real eq $passphrase)))
    {
        $self->_session->param('space', $self->{'space'});
        $self->_session->clear('passphrase');

        $self->_info("user $username logged in $self->{'space'} space");
        $self->_loadProfile($username);

        $res = 1;
    }
    else
    {
        $res = 0;
    }

    return $res;
}

################################################################################

sub _loggedIn
{
    my $self   = shift;

    my $flag   = shift;
    my $no_dbg = shift;

    if (defined $flag)
    {
        $self->{'logged_in'} = $flag;

        if ($self->{'logged_in'}) { $self->_session->param('~logged-in', 1); }
        else                      { $self->_session->clear(['~logged-in']);  }

        $self->_debug('(re)set logged_in: ', $self->{'logged_in'}) unless defined $no_dbg;
    }

    return $self->{'logged_in'};
}

###########################################################

sub _loadProfile
{
    my $self     = shift;

    my $username = shift;

    $self->{'userid'}              = $username;
    $self->{'profile'}{'username'} = $username;
    $self->{'profile'}{'space'}    = $self->_session->param('space');
}

################################################################################

sub auth
{
    my $ssl;
    my $seSSion;
    my $auth;
    my $changed;

    my $cgi        = shift;
    my $action     = shift;
    my $password   = shift;
    my $passphrase = shift;
    my $space      = shift;

    $changed = 0;

    # Ensure SSL connection is active
        $ssl = $cgi->https();
        $ssl = defined $ssl && $ssl eq 'on' ? 1 : 0;
    if ($ssl)
    {
        # SSL is used. We can safely continue.
        $seSSion = new CGI::Session(undef, $cgi, {'Directory' => TMP});

        # Delete seSSion if required
        if ($action == AUTH_LOGOUT)
        {
            $seSSion->delete();
            $seSSion->flush();

            $seSSion = new CGI::Session(undef, $cgi, {'Directory' => TMP});

            $changed = 1;
        }

        # SeSSion has been reused or the new one recreated.
        # Now authentication module can be used.
        $auth = new cgi({'CGI'     => $cgi,
                         'Session' => $seSSion});

        # Additional authentication parameters
        $auth->{'password'  } = $password;
        $auth->{'passphrase'} = $auth->_cgi->param('passphrase');
        $auth->{'space'     } = $space;

        # Perform a login attempt or read old seSSion information about
        # authentication
        if ($action != AUTH_LOGOUT)
        {
            if ($seSSion->param('~logged-in'))
            {
                if ($action == AUTH_LOGIN)
                {
                                    $auth->_loggedIn(0, 1);
                                    $auth->authenticate();
                    $changed = 1 if $auth->loggedIn();
                                    $auth->_loggedIn(1, 1);
                }
                else
                {
                    $cgi->delete('log_username');
                    $cgi->delete('log_password');

                    $auth->authenticate();
                }
            }
            else
            {
                if ($action == AUTH_LOGIN)
                {
                                    $auth->authenticate();
                    $changed = 1 if $auth->loggedIn();
                }
            }

            if ($action == AUTH_USE_SESSION)
            {
                $seSSion->param('passphrase', $passphrase) if defined $passphrase; # Save PGP passphrase if two-level authentication is used
            }
        }

        # Synchronize data in memory with the copy serialized by the
        # seSSion driver.
        # We should call it immediately after every important seSSion
        # update.
        $seSSion->flush();

        # Authentication status flag
        $auth->{'changed'} = $changed;
    }
    else
    {
        # SSL is disabled. Do not do anything.
        # It is too dangerous.
        $seSSion = undef;
        $auth    = undef;
    }

    return ($seSSion, $auth);
}

################################################################################

sub cookie_bake
{
    my $cgi     = shift;
    my $seSSion = shift;

    return \$cgi->cookie('-expires'  => COOKIE_EXPIRATION,
                         '-name'     => 'CGISESSID',
                         '-value'    => $seSSion->id,
                         '-secure'   => 1,
                         '-httponly' => 1);
}

################################################################################

sub header_get
{
    my $header;

    my $cgi     = shift;
    my $seSSion = shift;

    my $status  = shift;
    my $loc     = shift;
    my $cookie  = shift;

    unless (defined $cookie)
    {
        $cookie = defined $seSSion ? cookie_bake($cgi, $seSSion) :
                                     undef;
    }

    if (defined $loc)
    {
        $header = \$cgi->redirect('-status'   => $status,
                                  '-location' => $loc,
                                  '-cookie'   => $$cookie);
    }
    else
    {
        $header = \$cgi->header('-status'  => $status,
                                '-cookie'  => $$cookie,
                                '-charset' => 'utf-8');
    }

    return $header;
}

################################################################################

sub deinit
{
    my $cgi = shift;

    $cgi->delete_all();
}

return 1;
