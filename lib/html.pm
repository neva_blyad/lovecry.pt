=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
html.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2025 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare HTML package
package html;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use Locale::gettext;

# Our own modules
use i18n;

# Constants:
#
#  - HTML
use constant INDENT_WIDTH => 4;

use constant INDENT1 => ' ' x (1 * INDENT_WIDTH);
use constant INDENT2 => ' ' x (2 * INDENT_WIDTH);
use constant INDENT3 => ' ' x (3 * INDENT_WIDTH);
use constant INDENT4 => ' ' x (4 * INDENT_WIDTH);
use constant INDENT5 => ' ' x (5 * INDENT_WIDTH);
use constant INDENT6 => ' ' x (6 * INDENT_WIDTH);
use constant INDENT7 => ' ' x (7 * INDENT_WIDTH);
use constant INDENT8 => ' ' x (8 * INDENT_WIDTH);

use constant SITE_NAME => 'LoveCry.pt';

################################################################################
# HTML
################################################################################

sub header_get
{
    my $style;

    my $title = shift;
    my $space = shift;

    $style = defined $space ? "/$space/style.css" :
                              '/style.css';

    return \<< ";";
<!--
    Copyright (C) 2019-2025 НЕВСКИЙ БЛЯДИНА <neva_blyad[A_HERE]lovecry.pt>
                                            <neva_blyad[A_HERE]lovecri.es>

    This file is part of LoveCry.pt.

    LoveCry.pt is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    LoveCry.pt is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

    To see a copy of the license and information with full list of Non-AGPL
    components distributed by us please visit:
    www.lovecry.pt/copying/
-->

<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="$style">
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
    <title>$$title</title>
</head>

<body>

    <div id="table">

    <div id="header">
    <div id="menu">

;
}

################################################################################

sub footer_get
{
    my $footer;

    my $space = shift;

    $footer = '
    </div>
    </div>
    <div id="footer">
    <div id="menu">
';

    if (defined $space &&
        $space eq 'liberation')
    {
        $footer .= '
    <span id="menu"><a href="/liberation/maxi-liberation/">' . gettext('Maxi Liberation') . '</a></span>
    <span id="menu_delimiter">|</span>
';
    }

    $footer .= '
    <span id="menu"><a href="//keyring.lovecry.pt">' . gettext('PGP public key server') . '</a></span>
    <span id="menu_delimiter">|</span>
    <span id="menu"><a href="http://tox.lovecry.pt:8080">' . gettext('Tox name resolution server') . '</a></span>
    <span id="menu_delimiter">|</span>
    <span id="menu"><a href="https://gpt.lovecry.pt:8443">' . gettext('GPT/AI public chatbot') . '</a></span>
    <span id="menu_delimiter">|</span>

    <span id="menu"><a href="/copying/">' . gettext('Copying') . "</a></span>

    </div>
    </div>

    </div>

</body>

</html>\n";

    return \$footer;
}

################################################################################

sub title_get
{
    my @title = @_;

    return \join ' † ', @title;
}

return 1;
