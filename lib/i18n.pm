=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
i18n.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare CGI package
package i18n;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use CGI::Session;
use Geo::IP;
use Locale::gettext;
use POSIX;
use Tie::IxHash;

# Constants:
#
#  - i18n & l10n
use constant DIR     => 'locale/';
use constant DB      => 'dl/GeoLiteCity.dat';
use constant DOMAIN  => 'messages';
use constant CHARSET => 'UTF-8';

# Global Variables:
#
#  - i18n & l10n
tie our %locale, 'Tie::IxHash'; %locale = ();

%locale = ('en' => ['C',           'English'],
           'ru' => ['ru_RU.UTF-8', 'Русский']); # Supported translations

################################################################################
# i18n & l10n
################################################################################

sub init
{
    my $locale = shift;
    my $cgi    = shift;

    if (defined $$locale)
    {
        # Set locale using the DB user settings
        set($locale, undef);
    }
    else
    {
        # Check cookie is available
        my          $lang;
                    $lang = $cgi->cookie('lang');
        if (defined $lang)
        {
            # Set locale using the cookie user setting
            $locale = exists $locale{$lang} ? \$locale{$lang}[0] : \$locale{'en'}[0];
            set($locale, undef);
        }
        else
        {
            # Set locale using the user IP / hostname
            $locale = set(undef, \$cgi->remote_host());
        }
    }

    return $locale;
}

################################################################################

sub get
{
    # Get locale
    return setlocale(LC_MESSAGES);
}

###############################################################################

sub set
{
    my $locale = shift;
    my $host   = shift;

    # Determine the locale by IP / hostname.
    # (Otherwise, use the locale from the subroutine parameters.)
    if (defined $host)
    {
        my $geo;
        my $rec;

        $geo    = Geo::IP->open(DB, GEOIP_MEMORY_CACHE | GEOIP_CHECK_CACHE);
        $rec    = $geo->record_by_name($$host);
        $locale = defined $rec && exists $locale{lc $rec->country_code} ?
                      \$locale{lc $rec->country_code}[0] :
                      \$locale{'ru'}[0];
    }

    # Setup locale
    setlocale(LC_MESSAGES, $$locale);

    $ENV{'LANG'}        = $$locale;
    $ENV{'LC_MESSAGES'} = $$locale;

    if ($$locale eq 'C')
    {
        textdomain(NULL);
    }
    else
    {
        textdomain(DOMAIN);
        bindtextdomain(DOMAIN, DIR);
        bind_textdomain_codeset(DOMAIN, CHARSET);
    }

    return $locale;
}

return 1;
