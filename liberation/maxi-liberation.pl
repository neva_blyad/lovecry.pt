#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
maxi-liberation.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use CGI;
#use Cwd;
use Encode;
use File::Basename;
use File::Spec;
use Locale::gettext;
use Try::Tiny;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;

# Global Variables:
#
#  - CGI,
our $cgi         = undef;
our $cgi_space   = undef;
our $cgi_seSSion = undef;
our $cgi_auth    = undef;
our $cgi_auth_ok = undef;

#  - Database
our $db_locale   = undef;

#  - i18n
our $i18n_space  = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    $cgi_space = cgi::space_get();

    return cgi::init();
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    my $username = shift;

    return db::connect($username,
                       0 # Do not create database if it doesn't exist
                      );
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_select
{
    my $db     = shift;
    my $action = shift;

    $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# i18n
################################################################################

sub i18n_space_set
{
    Encode::_utf8_on($cgi_space);
    $i18n_space = gettext(ucfirst $cgi_space);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init  ();
    ($cgi_seSSion, $cgi_auth) = cgi_auth  ($cgi);
                                cgi_deinit($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect($cgi_auth->{'profile'}{'username'});

        if (defined $db)
        {
            #db_create($db);
            #db_insert($db);
            db_select($db);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir $path;

    i18n::init(\$db_locale, $cgi);
    i18n_space_set();
}
catch
{
    die "[2]: $_";
};

# HTML
print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};
print ${html::header_get(html::title_get(gettext('Maxi Liberation'),
                                         $i18n_space,
                                         html::SITE_NAME), $cgi_space)};

print html::INDENT1;

print qq(<span id="menu"><a href="/$cgi_space/">), gettext('Index'), q(</a></span>);
print q(<span id="menu_delimiter">|</span>);

print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);
print q(<span id="menu_delimiter">|</span>);

if ($cgi_auth_ok) { print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>); }
else              { print qq(<span id="menu"><a href="/$cgi_space/login/">), gettext('Log In'), q(</a></span>);                                                                                                                                                                                                                                                                                               }

print q(

    </div>
    </div>
    <div id="main">
    <div id="dummy">
    <div id="table">
    <div id="main">
    <div id="left">
    </div>
    <div id="centr640">

    <h1>), gettext('Maxi Liberation'), q(</h1>

    <h2>), gettext('What Is This?'), q(</h2>

    <p id="padding">), gettext('EPUB/FB2/FB3 book meta tag editor.'), q(</p>
    <p id="padding">), gettext('Maxi means maximum expression of digital text form.
Liberation is library.'), q(</p>
    <p id="padding">), gettext('The program is Free/Libre and Open Source Software (FLOSS). There is only one
right software license in the world.'), q(</p>
    <p id="padding">), gettext('The program is cross-platform: GNU/Linux, UNIX, M$ Windows, Apple macOS are
all supported. It is based on wxWidgets library, so the GUI is native for each
OS, for example, it uses GTK+ for GNU, MFC for Windows, Cocoa for macOS. No
fucking control emulation like Qt or Java does.'), q(</p>
    <p id="padding">), gettext('The program has been inspired and influenced by Ex Falso, popular meta tag
editor for audio files. Many thanks to its authors! Used it regularly for my
music collection.'), q(</p>
    <p id="padding">), gettext('The program is written in Perl. All modules like ZIP or XML are just bindings
to C dynamic libraries, so it is fast as C anyway.'), q(</p>
    <p id="padding">), gettext('Now EPUB, FB2 and FB3 formats are supported. AZW/MOBI are planned.'), q(</p>

    <h2>), gettext('Mascot'), q(</h2>

    <p id="centr_padding"><img src="/liberation/img/luna-libre_grey.png"></p>

    <p id="padding"><b>), gettext('Luna butterfly'), q(</b> ), gettext('is a project mascot.'), q(</p>
    <p id="padding">), gettext('whereideasoverlap is designer of original 16x16 icon.'), q(</p>

    <h2>), gettext('Screenshots'), q(</h2>

    <p id="centr_padding"><a href="/liberation/img/screenshot-linux.png"><img src="/liberation/img/thumbnails/screenshot-linux.png"></a></p>
    <p id="centr_padding"><i>), gettext('Maxi Liberation under GNU/Linux'), q(</i></p>

    <p id="centr_padding"><a href="/liberation/img/screenshot-win.png"><img src="/liberation/img/thumbnails/screenshot-win.png"></a></p>
    <p id="centr_padding"><i>), gettext('Maxi Liberation under M$ Windows'), q(</i></p>

    <p id="centr_padding"><a href="/liberation/img/screenshot-macos.png"><img src="/liberation/img/thumbnails/screenshot-macos.png"></a></p>
    <p id="centr_padding"><i>), gettext('Maxi Liberation under Apple macOS'), q(</i></p>

    <h2>), gettext('Screencast'), q(</h2>

    <p id="centr_padding">
    <video width="500" controls><source src="/liberation/video/screencast.webm" type="video/webm"></video>
    </p>

    <h2>), gettext('Download Binary'), q(</h2>

    <p>), gettext('The binary packages are available at these links.'), q(</p>

    <ul>
        <li><b>), gettext('Deb-based GNU/Linux distro:'), q(</b><br><a href="http://www.lovecry.pt/dl/maxi-liberation_0.27.deb">maxi-liberation_0.27.deb</a><br><span style="font-size: small;">(sha256) 82f693c0b0c6d7e7b9491cd91a308c369d6995c77cbc377f33ef8d1a162c3c97</span>
        <li><b>), gettext('RPM-based GNU/Linux distro:'), q(</b><br><a href="http://www.lovecry.pt/dl/maxi-liberation-0.27-2.noarch.rpm">maxi-liberation-0.27-2.noarch.rpm</a><br><span style="font-size: small;">(sha256) 938be5aa5464f8b5c44dec31313948c762aa191c85b038f3a500955d084eea94</span>
        <li><b>), gettext('Slackware and source-based GNU/Linux distros:'), q(</b><br><a href="http://www.lovecry.pt/dl/maxi-liberation-0.27.tgz">maxi-liberation-0.27.tgz</a><br><span style="font-size: small;">(sha256) 4464dc4e20ca1b01d29d0c7e06708b76c825dfe62c14de0e454dcd35ac395d94</span>
        <li><b>), gettext('M$ Windows'), q(</b><br>TODO
    </ul>

    <p>), gettext('Specific commands.'), q(</p>

    <ul>
        <li><b>), gettext('Apple macOS:'), q(</b><pre>$ wget https://gitlab.com/neva_blyad/maxi-liberation/-/blob/0.27/macos/maxi-liberation.rb
$ brew install --verbose maxi-liberation.rb
$ cpan -T -i Archive::Zip Config::Tiny File::Find::Rule File::HomeDir  \
      Locale::gettext Log::Log4perl MIME::Types Path::Tiny Tie::IxHash \
      XML::LibXML Wx</pre>
$ cp -R '/usr/local/Cellar/maxi-liberation/0.27/Maxi Liberation.app' /Applications/
    </ul>

    <h2>), gettext('Build From Scratch'), qq(</h2>

    <p id="centr_padding"><a href="https://www.gnu.org/licenses/gpl-3.0.en.html"><img src="/liberation/img/gplv3-with-text-136x68.png"></a></p>

    <p>), gettext('The source code is available at this link:'), q(</p>
    <p><a href="https://gitlab.com/neva_blyad/maxi-liberation/">https://gitlab.com/neva_blyad/maxi-liberation/</a></p>

    </div>
    <div id="right">
    </div>
    </div>
    </div>);

print ${html::footer_get($cgi_space)};
