#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
titles.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use CGI;
#use Cwd;
use DBI;
use Encode;
use File::Basename;
use File::Spec;
use File::stat;
use HTML::Entities;
use Locale::gettext;
use POSIX;
use Regexp::Common;
use Tie::IxHash;
use Try::Tiny;
use Unicode::String;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../lib/";
use lib "$path/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;
use img::cover;

# Constants:
#
#  - CGI,
use constant CGI_ACTION_VIEW      => 0;
use constant CGI_ACTION_DELETE    => 1;
use constant CGI_ACTION_RATE      => 2;

#  - Database,
use constant DB_SELECT_TITLES_CNT => 0;
use constant DB_SELECT_TITLES     => 1;
use constant DB_SELECT_LANG       => 2;
use constant DB_SELECT_ALIASES    => 3;

#  - HTML
use constant PAGE_LIMIT           => 88;

use constant PAGES_MAX            => 14;
use constant PAGES_LEFT           => int(PAGES_MAX / 2);
use constant PAGES_RIGHT          => int(PAGES_MAX / 2);

use constant COL_NUM              => 4;
use constant COL_IDX_LAST         => COL_NUM - 1;

# Global Variables:
#
#  - CGI,
our $cgi               = undef;
our $cgi_space         = undef;
our $cgi_seSSion       = undef;
our $cgi_auth          = undef;
our $cgi_auth_ok       = undef;

our %cgi_username      = ();
our %cgi_title_id      = ();
our %cgi_action        = ();

our %cgi_page          = ();

our %cgi_format        = ();
our %cgi_category      = ();
our %cgi_sort_by       = ();
our %cgi_sort_order    = ();

our %cgi_rating        = ();

our $cgi_pages         = undef;

#  - Database,
our $db                = undef;

our $db_sql_where      = undef;
our %db_sort_by        = ();
our %db_sort_order     = ();
our @db_sort_by        = ();
our @db_sort_order     = ();
our @db_sort_by_Alias  = ();

our $db_locale         = undef;

our $db_titles         = undef;
our $db_titles_cnt     = undef;
our $db_formats        = undef;
our $db_categories     = undef;
our @db_Aliases        = ();

our $db_format_Alias   = undef;
our $db_category_Alias = undef;
our $db_progress_Alias = undef;

our $db_title_id       = undef;

#  - i18n
our $i18n              = undef;
our $i18n_space        = undef;

#  - HTML
our $url_rating        = undef;
our $url_rating_       = undef;
our $url_pages         = undef;
our $url_pages_        = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    my $cgi;

    $cgi       = cgi::init();
    $cgi_space = cgi::space_get();

    $cgi_username  {'val'         } = $cgi->url_param('username');
    $cgi_username  {'verified_val'} = defined $cgi_username{'val'} && Unicode::String::utf8($cgi_username{'val'})->length() <= db::USERNAME_MAX_SIZE &&
                                                                      $cgi_username{'val'} =~ /^[a-z0-9_]+\z/ ?
                                          $cgi_username{'val'} : undef;

    $cgi_title_id  {'val'         } = $cgi->url_param('title_id');
    $cgi_title_id  {'verified_val'} = defined $cgi_title_id{'val'} && $cgi_title_id{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_title_id{'val'} > 0 ?
                                          $cgi_title_id{'val'} : undef;

    $cgi_action    {'val'         } = $cgi->url_param('action');
    $cgi_action    {'verified_val'} = defined $cgi_action{'val'} && grep({ $cgi_action{'val'} eq $_ } ('delete',
                                                                                                       'rate')) ?
                                          $cgi_action{'val'} : undef;

    $cgi_page      {'val'         } = $cgi->url_param('page');
    $cgi_page      {'verified_val'} = defined $cgi_page{'val'} && $cgi_page{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_page{'val'} > 1 ?
                                          $cgi_page{'val'} : undef;

    $cgi_rating    {'val'         } = $cgi->url_param('rating');
    $cgi_rating    {'verified_val'} = defined $cgi_rating{'val'} && $cgi_rating{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_rating{'val'} >= 1 &&
                                                                                                                    $cgi_rating{'val'} <= db::RATING_MAX ?
                                          $cgi_rating{'val'} : undef;

    return $cgi;
}

################################################################################

sub cgi_postinit
{
    $cgi_format    {'val'         } = [$cgi->url_param($db_format_Alias)];
    $cgi_format    {'verified_val'} = defined $cgi_format{'val'}->[0] ?
                                          $cgi_format{'val'} : [];

    $cgi_category  {'val'         } = [$cgi->url_param($db_category_Alias)];
    $cgi_category  {'verified_val'} = defined $cgi_category{'val'}->[0] ?
                                          $cgi_category{'val'} : [];

    $cgi_sort_by   {'val'         } = $cgi->url_param('sort_by');
    $cgi_sort_by   {'verified_val'} = defined $cgi_sort_by{'val'} && grep({ $cgi_sort_by{'val'} eq $_ } @db_sort_by_Alias) ?
                                          $cgi_sort_by{'val'} : undef;

    $cgi_sort_order{'val'         } = $cgi->url_param('sort_order');
    $cgi_sort_order{'verified_val'} = defined $cgi_sort_order{'val'} && grep({ $cgi_sort_order{'val'} eq $_ } @db_sort_order) ?
                                          $cgi_sort_order{'val'} : undef;
}

################################################################################

sub cgi_action_set
{
    my $cgi = shift;

    if (defined $cgi_action{'verified_val'})
    {
        if ($cgi_action{'verified_val'} eq 'delete') { $cgi_action{'final_val'} = CGI_ACTION_DELETE; }
        else                                         { $cgi_action{'final_val'} = CGI_ACTION_RATE;   }
    }
    else                                             { $cgi_action{'final_val'} = CGI_ACTION_VIEW;   }
}

################################################################################

sub cgi_page_set
{
    my $cgi = shift;

    if (defined $db_titles_cnt)
    {
            $cgi_pages = ceil($db_titles_cnt / PAGE_LIMIT);
        if ($cgi_pages == 0)
        {
            $cgi_page{'final_val'} = 0;
        }
        else
        {
            $cgi_page{'final_val'} = defined $cgi_page{'verified_val'} &&
                                             $cgi_page{'verified_val'} <= $cgi_pages ?
                $cgi_page{'verified_val'} :
                1;
        }
    }
}

################################################################################

sub cgi_format_set
{
    my $cgi = shift;

    $cgi_format{'final_val'} = [];

    foreach my $format_idx (0 .. $#{$cgi_format{'verified_val'}})
    {
        next if $cgi_format{'verified_val'}->[$format_idx] eq '' ||
                Unicode::String::utf8($cgi_format{'verified_val'}->[$format_idx])->length() > db::FIELD_SINGLE_LINE_MAX_SIZE ||
                $cgi_format{'verified_val'}->[$format_idx] =~ /\n|\r/;

        push @{$cgi_format{'final_val'}}, $cgi_format{'verified_val'}->[$format_idx];
    }
}

################################################################################

sub cgi_category_set
{
    my $cgi = shift;

    $cgi_category{'final_val'} = [];

    foreach my $category_idx (0 .. $#{$cgi_category{'verified_val'}})
    {
        next if $cgi_category{'verified_val'}->[$category_idx] eq '' ||
                Unicode::String::utf8($cgi_category{'verified_val'}->[$category_idx])->length() > db::FIELD_SINGLE_LINE_MAX_SIZE ||
                $cgi_category{'verified_val'}->[$category_idx] =~ /\n|\r/;

        push @{$cgi_category{'final_val'}}, $cgi_category{'verified_val'}->[$category_idx];
    }
}

################################################################################

sub cgi_sort_by_set
{
    my $cgi = shift;

    $cgi_sort_by{'final_val'} = defined $cgi_sort_by{'verified_val'} ?
        $cgi_sort_by{'verified_val'} :
        $db_sort_by{'author'}[0];
}

################################################################################

sub cgi_sort_order_set
{
    my $cgi = shift;

    $cgi_sort_order{'final_val'} = defined $cgi_sort_order{'verified_val'} ?
        $cgi_sort_order{'verified_val'} :
        'ASC';
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_postinit
{
    my ($author_Alias, $title_Alias, $publisher_Alias, $isbn_Alias, $format_Alias,
            $date_Alias, $rating_Alias, $category_Alias, $progress_Alias, $comment_Alias);
    my ($author_Alias_, $title_Alias_, $publisher_Alias_, $isbn_Alias_, $format_Alias_,
            $date_Alias_, $rating_Alias_, $category_Alias_, $progress_Alias_, $comment_Alias_);

    if (@db_Aliases > 0)
    {
        $author_Alias     = encode_entities($db_Aliases[ 0], '<>&"');
        $title_Alias      = encode_entities($db_Aliases[ 1], '<>&"');
        $publisher_Alias  = encode_entities($db_Aliases[ 2], '<>&"');
        $isbn_Alias       = encode_entities($db_Aliases[ 3], '<>&"');
        $format_Alias     = encode_entities($db_Aliases[ 4], '<>&"');
        $date_Alias       = encode_entities($db_Aliases[ 5], '<>&"');
        $rating_Alias     = 'rating';
        $category_Alias   = encode_entities($db_Aliases[ 6], '<>&"');
        $progress_Alias   = encode_entities($db_Aliases[ 7], '<>&"');

        $author_Alias_    = encode_entities($db_Aliases[12], '<>&');
        $title_Alias_     = encode_entities($db_Aliases[13], '<>&');
        $publisher_Alias_ = encode_entities($db_Aliases[14], '<>&');
        $isbn_Alias_      = encode_entities($db_Aliases[15], '<>&');
        $format_Alias_    = encode_entities($db_Aliases[16], '<>&"');
        $date_Alias_      = encode_entities($db_Aliases[17], '<>&');
        $rating_Alias_    = gettext('Rating');
        $category_Alias_  = encode_entities($db_Aliases[18], '<>&"');
        $progress_Alias_  = encode_entities($db_Aliases[19], '<>&');
    }
    else
    {
        $author_Alias     = 'Author';
        $title_Alias      = 'Title';
        $publisher_Alias  = 'Publisher';
        $isbn_Alias       = 'ISBN';
        $format_Alias     = 'Format';
        $date_Alias       = 'Date';
        $rating_Alias     = 'Rating';
        $category_Alias   = 'Category';
        $progress_Alias   = 'Progress';

        $author_Alias_    = gettext('Author');
        $title_Alias_     = gettext('Title');
        $publisher_Alias_ = gettext('Publisher');
        $isbn_Alias_      = gettext('ISBN');
        $format_Alias_    = gettext('Format');
        $date_Alias_      = gettext('Date');
        $rating_Alias_    = gettext('Rating');
        $category_Alias_  = gettext('Category');
        $progress_Alias_  = gettext('Progress');
    }

    tie our %db_sort_by,    'Tie::IxHash';
    tie our %db_sort_order, 'Tie::IxHash';

    %db_sort_by =
    (
        'author'    => [$author_Alias,    $author_Alias_   ],
        'title'     => [$title_Alias,     $title_Alias_    ],
        'publisher' => [$publisher_Alias, $publisher_Alias_],
        'isbn'      => [$isbn_Alias,      $isbn_Alias_     ],
        'format'    => [$format_Alias,    $format_Alias_   ],
        'date'      => [$date_Alias,      $date_Alias_     ],
        'rating'    => [$rating_Alias,    $rating_Alias_   ],
        'category'  => [$category_Alias,  $category_Alias_ ],
        'progress_' => [$progress_Alias,  $progress_Alias_ ]
    );

    %db_sort_order = ('ASC'  => gettext('Asc.' ),
                      'DESC' => gettext('Desc.'));

    @db_sort_by    = keys %db_sort_by;
    @db_sort_order = keys %db_sort_order;

    @db_sort_by_Alias = ($author_Alias, $title_Alias, $publisher_Alias, $isbn_Alias,
                            $format_Alias, $date_Alias, $rating_Alias, $category_Alias,
                            $progress_Alias);


    Encode::_utf8_on($format_Alias);
    Encode::_utf8_on($category_Alias);
    Encode::_utf8_on($progress_Alias);

    $db_format_Alias   = lc $format_Alias;
    $db_category_Alias = lc $category_Alias;
    $db_progress_Alias = lc $progress_Alias;
}

################################################################################

sub db_connect
{
    my $db;

    my $username = shift;

    return undef unless defined $username;

    $db = db::connect($username,
                      0 # Do not create database if it doesn't exist
                     );
    img::cover::init($username) if defined $db;

    return $db;
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_select
{
    my $sql;
    my $query;

    my $db     = shift;
    my $action = shift;

    if ($action == DB_SELECT_TITLES_CNT)
    {
        # Form the start part of the SQL query
        $sql = << ';';
SELECT          COUNT(*)
FROM            titles
;

        if (@{$cgi_format{'final_val'}} > 0)
        {
            $sql .= << ';';

JOIN            formats
ON              titles.format_id   = formats.format_id
;
#USING           (format_id)
        }

        if (@{$cgi_category{'final_val'}} > 0)
        {
            $sql .= "\n" if @{$cgi_format{'final_val'}} == 0;
            $sql .= << ';';
JOIN            categories
ON              titles.category_id = categories.category_id
;
#USING           (category_id)
        }

        $db_sql_where = '';

        # Add format to the WHERE clause
        if (@{$cgi_format{'final_val'}} > 0)
        {
            if (@{$cgi_format  {'final_val'}} > 1 &&
                @{$cgi_category{'final_val'}} > 0)
            {
                $db_sql_where .= '
WHERE           (format = ?';
                $db_sql_where .= ' OR
                 format = ?' for 1 .. $#{$cgi_format{'final_val'}};
                $db_sql_where .= ')';
            }
            else
            {
                $db_sql_where .= '
WHERE           format = ?';
                $db_sql_where .= ' OR
                format = ?'  for 1 .. $#{$cgi_format{'final_val'}};
            }

            $db_sql_where .= << ';';

;
        }

        # Add category to the WHERE clause
        if (@{$cgi_category{'final_val'}} > 0)
        {
            if (@{$cgi_format  {'final_val'}} > 0 &&
                @{$cgi_category{'final_val'}} > 1)
            {
                $db_sql_where .= '
AND             (category = ?';
                $db_sql_where .= ' OR
                 category = ?' for 1 .. $#{$cgi_category{'final_val'}};
                $db_sql_where .= ')';
            }
            else
            {
                $db_sql_where .= @{$cgi_format{'final_val'}} > 0 ? '
AND             category = ?' : '
WHERE           category = ?';
                $db_sql_where .= ' OR
                category = ?'  for 1 .. $#{$cgi_category{'final_val'}};
            }

            $db_sql_where .= << ';';

;
        }

        # Add format to the WHERE clause.
        # Add category to the WHERE clause.
        $sql .= $db_sql_where;

        $query = $db->prepare($sql);

        foreach my $format_idx   (0 .. $#{$cgi_format  {'final_val'}})
        { $query->bind_param($format_idx + 1,                                  $cgi_format  {'final_val'}->[$format_idx],   DBI::SQL_VARCHAR); }
        foreach my $category_idx (0 .. $#{$cgi_category{'final_val'}})
        { $query->bind_param($#{$cgi_format{'final_val'}} + 2 + $category_idx, $cgi_category{'final_val'}->[$category_idx], DBI::SQL_VARCHAR); }

                          $query->execute();
        $db_titles_cnt = ($query->fetchrow_array())[0];
    }
    elsif ($action == DB_SELECT_TITLES)
    {
        return if defined $cgi_title_id{'val'} ||
                  defined $cgi_action  {'val'};

        return if defined $cgi_rating{'val'};

        # Form the start part of the SQL query
        $sql = << ";";
SELECT          title_id, author, title, publisher,
                    isbn, format$i18n AS format, date, rating,
                    category$i18n AS category, progress, progress_begin, progress_end,
                    CAST(progress_begin AS REAL) / progress_end AS progress_
FROM            titles

JOIN            authors
ON              titles.author_id    = authors.author_id
JOIN            publishers
ON              titles.publisher_id = publishers.publisher_id
JOIN            formats
ON              titles.format_id    = formats.format_id
JOIN            categories
ON              titles.category_id  = categories.category_id
;
#USING           (author_id)
#USING           (publisher_id)
#USING           (format_id)
#USING           (category_id)

        $sql .= $db_sql_where;

        # Add the ORDER clause
        $sql .= '
ORDER BY ';

        foreach my $col (@db_sort_by)
        {
            next if $db_sort_by{$col}[0] ne $cgi_sort_by{'final_val'};
            $sql .= "       $col $cgi_sort_order{'final_val'}";
            last;
        }

        foreach my $col (@db_sort_by)
        {
            next if $db_sort_by{$col}[0] eq $cgi_sort_by{'final_val'};
            $sql .= ", $col";
        }

        # Add the LIMIT clause
        $sql .= '

LIMIT ' . PAGE_LIMIT . ($cgi_page{'final_val'} > 1 ? ' OFFSET ' . ($cgi_page{'final_val'} - 1) * PAGE_LIMIT :
                                                     '') . "\n";

        $query = $db->prepare($sql);

        foreach my $format_idx   (0 .. $#{$cgi_format  {'final_val'}})
        { $query->bind_param($format_idx + 1,                                  $cgi_format  {'final_val'}->[$format_idx],   DBI::SQL_VARCHAR); }
        foreach my $category_idx (0 .. $#{$cgi_category{'final_val'}})
        { $query->bind_param($#{$cgi_format{'final_val'}} + 2 + $category_idx, $cgi_category{'final_val'}->[$category_idx], DBI::SQL_VARCHAR); }

                     $query->execute();
        $db_titles = $query->fetchall_arrayref();

        $db_formats = $db->selectall_arrayref(<< ";");
SELECT    format, format$i18n, CASE WHEN title_id IS NOT NULL
                               THEN      COUNT(*)
                               ELSE      0
                               END AS    formats
FROM      formats
LEFT JOIN titles
ON        formats.format_id = titles.format_id

GROUP BY  formats.format_id

ORDER BY  formats.format_id
;
#USING     (format_id)

        $db_categories = $db->selectall_arrayref(<< ";");
SELECT    category, category$i18n, CASE WHEN title_id IS NOT NULL
                                   THEN      COUNT(*)
                                   ELSE      0
                                   END AS    titles
FROM      categories
LEFT JOIN titles
ON        categories.category_id = titles.category_id

GROUP BY  categories.category_id

ORDER BY  categories.category_id
;
#USING     (category_id)
    }
    elsif ($action == DB_SELECT_LANG)
    {
        $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
    }
    #elsif ($action == DB_SELECT_ALIASES)
    else
    {
        $db->{'PrintWarn'}  = 0;
        $db->{'PrintError'} = 0;
        @db_Aliases = $db->selectrow_array(<< ";");
SELECT author, title, publisher, isbn,
           format, date, category, progress,
           page_menu_products, no_proposal,
           pages, locations,
       author$i18n, title$i18n, publisher$i18n, isbn$i18n,
           format$i18n, date$i18n, category$i18n, progress$i18n,
           page_menu_products$i18n, no_proposal$i18n,
           pages$i18n, locations$i18n
FROM   Aliases
;
        $db->{'PrintWarn'}  = 1;
        $db->{'PrintError'} = 1;
    }
}

################################################################################

sub db_update
{
    my $db = shift;

    return unless defined $cgi_title_id{'verified_val'};

    return unless defined $cgi_rating{'verified_val'};

    db::title_update($db,

                     undef,
                     undef,
                     undef,
                     undef,
                     undef,
                     undef,
                     undef,
                     $cgi_rating{'verified_val'},
                     undef,
                     undef,
                     undef,
                     undef,
                     undef,

                     $cgi_title_id{'verified_val'});

    $db_title_id = $cgi_title_id{'verified_val'};
}

################################################################################

sub db_delete
{
    my $query;

    my $db = shift;

    return unless defined $cgi_title_id{'verified_val'};

    return if defined $cgi_rating{'val'};

    # I commented out this heredoc because it breaks xgettext.
    # I don't know why this shit happens.
#    $query = $db->prepare(<< ';');
#DELETE FROM titles
#WHERE       title_id = ?
#;
    $query = $db->prepare('DELETE FROM titles
WHERE       title_id = ?');

    $query->bind_param(1, $cgi_title_id{'verified_val'}, DBI::SQL_INTEGER);
    $query->execute();

    db::authors_publishers_formats_categories_delete($db);

    $db_title_id = $cgi_title_id{'verified_val'};

    img::cover::dir_change($cgi_username{'verified_val'});
    unlink "$db_title_id.png";
    img::cover::dir_back_change();
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# i18n
################################################################################

sub i18n_init
{
    i18n::init(\$db_locale, $cgi);

    $i18n = i18n::get();
    $i18n = $i18n eq 'C' ? '' :
                           '_' . substr $i18n, 0, 2;
}

################################################################################

sub i18n_space_set
{
    Encode::_utf8_on($cgi_space);
    $i18n_space = gettext(ucfirst $cgi_space);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init();
                                cgi_action_set  ($cgi);
    ($cgi_seSSion, $cgi_auth) = cgi_auth        ($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/../$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect($cgi_auth->{'profile'}{'username'});

        if (defined $db)
        {
            #db_create($db);
            #db_insert($db);
            db_select($db, DB_SELECT_LANG);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir "$path/../";

    i18n_init();
    i18n_space_set();
}
catch
{
    die "[2]: $_";
};

# Database.
#
# Get Aliases of the viewed profile.
try
{
    chdir $path;

    $db = db_connect($cgi_username{'verified_val'});

    if (defined $db)
    {
        #db_create($db);
        #db_insert($db);
        db_select($db, DB_SELECT_ALIASES);
    }

    chdir "$path/../";

    db_postinit();
}
catch
{
    die "[3]: $_";
};

# CGI.
#
# Handle those GET-parameters which we couldn't check before
# doing some database queries.
try
{
    chdir $path;

    cgi_postinit();

    cgi_format_set    ($cgi);
    cgi_category_set  ($cgi);
    cgi_sort_by_set   ($cgi);
    cgi_sort_order_set($cgi);
}
catch
{
    die "[4]: $_";
};

# Database.
#
# View books, add books, rate books of the viewed profile if
# it is user's profile. If it doesn't, just view.
try
{
    if (defined $db)
    {
        if    ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
        {
            db_select($db, DB_SELECT_TITLES_CNT);
        }
        elsif ($cgi_action{'final_val'} == CGI_ACTION_DELETE &&
               $cgi_auth_ok &&
               $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
               $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            db_delete($db);
        }
        elsif ($cgi_action{'final_val'} == CGI_ACTION_RATE &&
               $cgi_auth_ok &&
               $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
               $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            db_update($db);
        }
    }
}
catch
{
    die "[5]: $_";
};

# CGI
try
{
    cgi_page_set($cgi);
}
catch
{
    die "[6]: $_";
};

# Database.
#
# View books.
try
{
    if (defined $db)
    {
        if ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
        {
            db_select($db, DB_SELECT_TITLES);
        }

        db_disconnect($db);
    }

    chdir "$path/../";
}
catch
{
    die "[7]: $_";
};

# HTML
$CGI::USE_PARAM_SEMICOLONS = 0;

$cgi->delete('username');
$cgi->delete('title_id');
$cgi->delete('action');
$cgi->delete('rating');

    $url_rating = $cgi->query_string();
if ($url_rating eq '')
{
    $url_rating_ = '';
}
else
{
    $url_rating  = "?$url_rating";
    $url_rating_ = encode_entities($url_rating, '<>&"');
}

$cgi->delete('page');

    $url_pages = $cgi->query_string();
if ($url_pages eq '')
{
    $url_pages_ = '';
}
else
{
    $url_pages  = "?$url_pages";
    $url_pages_ = encode_entities($url_pages, '<>&"');
}

if (defined $db_title_id)
{
    # There is deleted or rated title. Get redirection here.
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, "/$cgi_space/$cgi_username{'verified_val'}/$url_rating", undef)};
}
elsif (!defined $db_titles) # No page with such username or no permission to do rate action
{
    # So 404
    print ${cgi::header_get($cgi, $cgi_seSSion, 404, undef, undef)};
}
else
{
    my  $td_idx_last;
    my ($rowspan1, $rowspan2);
    my ($pages_begin, $pages_end);

    # Everything is OK. Show the titles page.
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};
    print ${html::header_get(html::title_get($cgi_username{'verified_val'},
                                             $i18n_space,
                                             html::SITE_NAME), $cgi_space)};

    # Print filter form. It can be used for the title sorting.
    $rowspan1 = $#$db_formats    == -1 ? ' rowspan="' . ($#$db_categories == -1 ? 2 : $#$db_categories + 2) . '"' : '';
    $rowspan2 = $#$db_categories == -1 ? ' rowspan="2"'                                                           : '';

    print html::INDENT1, qq(<form id="filters">
        <table id="filters">
            <tr><th$rowspan1 id="format">), gettext('Format'), qq(</th><th$rowspan2 id="category">), gettext('Category'), q(</th><th colspan="2" id="sort">), gettext('Sort By'), qq(</th>\n);

    if ($#$db_formats == -1 && $#$db_categories == -1)
    {
        $td_idx_last = 0;
    }
    else
    {
        $td_idx_last = $#$db_formats > $#$db_categories ? $#$db_formats :
                                                          $#$db_categories;
    }

    $rowspan1 = '';

    foreach my $td (0 .. $td_idx_last)
    {
        my ($val, $val_i18n, $val_cnt);
        my  $id;
        my  $avail;

        # Output next table cell:
        print << ";";
            <tr>
;

        # Output next table cell:
        #  - format filter,
        if ($td <= $#$db_formats)
        {
            ($val, $val_i18n, $val_cnt) = @{$db_formats->[$td]};

            if ($val_cnt > 0)
            {
                $id    = '';
                $avail = grep({ $val eq $_ } @{$cgi_format{'final_val'}}) ? ' checked'  : '';
            }
            else
            {
                $id    = ' id="grey"';
                $avail = ' disabled';
            }

            $val      = encode_entities($val,      '<>&"');
            $val_i18n = encode_entities($val_i18n, '<>&');

            $rowspan1 = ' rowspan="' . ($#$db_categories - $td + 1) . '"' if $td == $#$db_formats && $td < $#$db_categories;

            print << ";";
                <td$rowspan1 id="format"><label$id><input type="checkbox" name="$db_format_Alias" value="$val"$avail>$val_i18n ($val_cnt)</label></td>
;
        }

        # gettext() hack comment """

        #  - category filter,
        if ($td <= $#$db_categories)
        {
            ($val, $val_i18n, $val_cnt) = @{$db_categories->[$td]};

            if ($val_cnt > 0)
            {
                $id    = '';
                $avail = grep({ $val eq $_ } @{$cgi_category{'final_val'}}) ? ' checked'  : '';
            }
            else
            {
                $id    = ' id="grey"';
                $avail = ' disabled';
            }

            $val      = encode_entities($val,      '<>&"');
            $val_i18n = encode_entities($val_i18n, '<>&');

            print << ";";
                <td id="category"><label$id><input type="checkbox" name="$db_category_Alias" value="$val"$avail>$val_i18n ($val_cnt)</label></td>
;
        }

        #  - sort by, sort order,
        if ($td == 0)
        {
            print << ';';
                <td id="sort_by">
                    <select name="sort_by" autofocus>
;

            foreach my $sort_by (@db_sort_by)
            {
                my $sel;

                $sel = $db_sort_by{$sort_by}[0] eq $cgi_sort_by{'final_val'} ? ' selected' : '';
                print << ";";
                        <option value="$db_sort_by{$sort_by}[0]"$sel>$db_sort_by{$sort_by}[1]
;
            }

            print << ';';
                    </select>
                </td>
                <td id="sort_order">
                    <select name="sort_order">
;

            foreach my $sort_order (@db_sort_order)
            {
                my $sel;

                $sel = $sort_order eq $cgi_sort_order{'final_val'} ? ' selected' : '';
                print << ";";
                        <option value="$sort_order"$sel>$db_sort_order{$sort_order}
;
            }

            print << ';';
                    </select>
                </td>
;
        }
    }

    print html::INDENT2, q(</table>
        <input type="submit" value="), gettext('Filter'), qq(">
    </form>\n\n);

    print html::INDENT1;

    if ($cgi_auth_ok &&
        $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
        $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
    {
        print qq(<span id="menu"><a href="/$cgi_space/$cgi_username{'verified_val'}/add/">), gettext('Add'), q(</a></span>);
        print  q(<span id="menu_delimiter">|</span>);
    }

    print qq(<span id="menu"><a href="/$cgi_space/">), gettext('Index'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    if ($cgi_auth_ok) { print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>); }
    else              { print qq(<span id="menu"><a href="/$cgi_space/login/">), gettext('Log In'), q(</a></span>);                                                                                                                                                                                                                                                                                              }

    print << ';';


    </div>
    </div>
;

    # Page menu
    if ($cgi_page{'final_val'} > 0)
    {
        my $tmp;

        $pages_begin = html::INDENT1 . q(<div id="header">
    <div id="pages">

    ) . gettext('You are viewing') . qq( <span id="profile"><span id="icon"><a href="/$cgi_space/$cgi_username{'verified_val'}/profile/">💉</a></span><a href="/$cgi_space/$cgi_username{'verified_val'}/">$cgi_username{'verified_val'}</a></span>) . gettext("'s pages:");

        # First to current pages
        $tmp = $cgi_page{'final_val'} - PAGES_LEFT;

        if ($tmp < 1)
        {
            $tmp = 1;
        }
        elsif ($tmp > 1)
        {
            $pages_begin .= '<span id="page">...</span>';
        }

        foreach my $page ($tmp .. $cgi_page{'final_val'} - 1)
        {
            $pages_begin .= qq(<span id="page"><a href="/$cgi_space/$cgi_username{'verified_val'}/$url_pages_) .
                ($url_pages_ eq '' ? '?' : '&amp;') . qq(page=$page">$page</a></span>);
        }

        # Current page
        $pages_begin .= qq(<span id="page">$cgi_page{'final_val'}</span>);

        # Current to last pages
        $tmp = $cgi_page{'final_val'} + PAGES_RIGHT;

        if ($tmp > $cgi_pages)
        {
            $tmp = $cgi_pages;
        }

        foreach my $page ($cgi_page{'final_val'} + 1 .. $tmp)
        {
            $pages_begin .= qq(<span id="page"><a href="/$cgi_space/$cgi_username{'verified_val'}/$url_pages_) .
                ($url_pages_ eq '' ? '?' : '&amp;') . qq(page=$page">$page</a></span>);
        }

        if ($tmp < $cgi_pages)
        {
            $pages_begin .= '<span id="page">...</span>';
        }
        else
        {
            $pages_begin .= '.';
        }

        $tmp = ($cgi_page{'final_val'} - 1) * PAGE_LIMIT;

        $pages_begin .= '

    </div>
    </div>
    <div id="header">
    <div id="cnt">

    ' . gettext('Showing ') . ($tmp + 1)  . '–' . ($tmp + scalar @$db_titles) .
            (@db_Aliases > 0 ? encode_entities($db_Aliases[20], '<>&') : gettext(' books of ')) . "$db_titles_cnt.";

        $pages_end = << ';';


    </div>
    </div>
;
    }
    else
    {
        $pages_begin = '';
        $pages_end   = '';
    }

    print $pages_begin, $pages_end;

    print << ';';
    <div id="main">
    <div id="centr">

;

    # Check we have any books
    if (@$db_titles == 0)
    {
        # No books case
        print html::INDENT1, '<div id="centr_full"><h1>',
            (@db_Aliases > 0 ? encode_entities($db_Aliases[21], '<>&') : gettext('No Liberation')), '</h1></div>';
    }
    else
    {
        my $title_idx;
        my $row_idx_last;

        # Print main table with all titles
        print << ';';
    <table id="titles">
;

        $title_idx    = 0;
        $row_idx_last = int($#$db_titles / COL_NUM);

        foreach my $row (0 .. $row_idx_last)
        {
            my $col_idx_last;

            my  @title_id;
            my (@author, @title, @publisher, @isbn,
                    @format, @date, @cover, @rating,
                    @category_i18n, @progress, @progress_begin, @progress_end);

            my @ts;

            my $colspan;

            # Get data from database — next COL_LUM numbers
            $col_idx_last = $row == $row_idx_last ? $#$db_titles % COL_NUM :
                                                    COL_IDX_LAST;

            foreach my $col (0 .. $col_idx_last)
            {
                my $cover_path;
                my $stat;

                # Read database handler
                 $title_id[$col] = encode_entities(@{$db_titles->[$title_idx + $col]}[0], '<>&"');
                ($author[$col], $title[$col], $publisher[$col], $isbn[$col],
                     $format[$col], $date[$col], $rating[$col], $category_i18n[$col],
                     $progress[$col], $progress_begin[$col], $progress_end[$col]) = map encode_entities($_, '<>&'), @{$db_titles->[$title_idx + $col]}[1..11];

                # Calculate timestamp for the cover
                $cover_path = "$cgi_space/" . img::cover::DIR . "$cgi_username{'verified_val'}/$title_id[$col].png";
                $stat       = stat $cover_path;
                $ts[$col]   = ($stat) ? '?' . $stat->mtime() :
                                        '';
            }

            $colspan = db::RATING_MAX + 1;

            # Print information — next COL_LUM numbers:
            #  - cover,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<th colspan="$colspan" id="cover"><a href="/$cgi_space/$cgi_username{'verified_val'}/$title_id[$col]/"><img src="/$cgi_space/), img::cover::DIR, qq($cgi_username{'verified_val'}/$title_id[$col].png$ts[$col]" id="cover"></a></th>\n); }
            foreach my $col ($col_idx_last + 1 .. COL_IDX_LAST) { print html::INDENT3, qq(<td colspan="$colspan" rowspan="8" id="cover"></td>\n); }

            #  - author,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td colspan="$colspan" id="main_hdr_lvl2">$author[$col]</td>\n); }

            #  - title,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td colspan="$colspan" id="main"><a href="/$cgi_space/$cgi_username{'verified_val'}/$title_id[$col]/">$title[$col]</a></td>\n); }

            #  - publisher,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td colspan="$colspan" id="main_centr">$publisher[$col]</td>\n); }

            #  - ISBN,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td colspan="$colspan" id="main_centr">$isbn[$col]</td>\n); }

            #  - date,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)                { print html::INDENT3, qq(<td colspan="$colspan" id="main_centr">$date[$col]</td>\n); }

            #  - format,
            #  - rating,
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)
            {
                print html::INDENT3, qq(<td id="format">$format[$col]</td>\n);

                foreach my $rating (1 .. db::RATING_MAX)
                {
                    my ($id, $icon);
                    my ($a_href, $a_href_);

                    # <td id="...">
                    if    ($rating == 1)              { $id = 'rating_left';  }
                    elsif ($rating == db::RATING_MAX) { $id = 'rating_right'; }
                    else                              { $id = 'rating';       }

                    # ICO picture to show
                    if ($cgi_space eq 'liberation')
                    {
                        $icon = $rating <= $rating[$col] ? 'luna-libre.ico' :
                                                           'luna.ico';
                    }
                    #elsif ($cgi_space eq 'hair')
                    else
                    {
                        $icon = $rating <= $rating[$col] ? 'cute_heart.ico' :
                                                           'cute_heart-disabled.ico';
                    }

                    # Allow rate links if username is logged in
                    if ($cgi_auth_ok &&
                        $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
                        $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
                    {
                        $a_href  = qq(<a href="/$cgi_space/$cgi_username{'verified_val'}/$title_id[$col]/rate/$rating/$url_rating_">);
                        $a_href_ =  q(</a>);
                    }
                    else
                    {
                        $a_href  = '';
                        $a_href_ = '';
                    }

                    print html::INDENT3, qq(<td id="$id">$a_href<img src="/$cgi_space/img/$icon" id="rating">$a_href_</td>\n);
                }
            }

            #  - category,
            #  - progress
                                                                  print html::INDENT2, qq(<tr>\n);
            foreach my $col (0 .. $col_idx_last)
            {
                my ($td1, $td2);
                my  $units;

                if ($progress[$col] == db::PROGRESS_DIS)
                {
                    $td1  = qq(colspan="6" id="category_if_no_$db_progress_Alias");
                }
                else
                {
                    $td1  = 'id="category"';

                    $td2  = 'colspan="5" id="';
                    $td2 .= ($progress_begin[$col] == $progress_end[$col]) ? "${db_progress_Alias}_liberated" :
                                                                             "$db_progress_Alias";
                    $td2 .= '"';

                    if    ($progress[$col] == db::PROGRESS_PAGES) { $units = @db_Aliases > 0 ? encode_entities($db_Aliases[22], '<>&') : gettext(' p.');   }
                    elsif ($progress[$col] == db::PROGRESS_LOC)   { $units = @db_Aliases > 0 ? encode_entities($db_Aliases[23], '<>&') : gettext(' loc.'); }
                }

                print html::INDENT3, qq(<td $td1>$category_i18n[$col]</td>\n);
                print html::INDENT3, qq(<td $td2>$progress_begin[$col]/$progress_end[$col]$units</td>\n) if $progress[$col] != db::PROGRESS_DIS;
            }

            $title_idx += COL_NUM;
        }

        # Print end of the titles table
        print << ';';
    </table>
;
    }

    print $pages_end, $pages_begin, "\n";

    print ${html::footer_get($cgi_space)};
}

# CGI
try
{
    cgi_deinit($cgi);
}
catch
{
    die "[6]: $_";
};
