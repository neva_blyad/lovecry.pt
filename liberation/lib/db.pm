=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
db.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare database package
package db;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use DBI;
use Digest::SHA;
use Tie::IxHash;

# Constants:
#
#  - Database
use constant DIR                        => 'db/';

use constant TITLES_MAX                 => 8814;

use constant USERNAME_MAX_SIZE          =>     18;
use constant PASSWORD_MAX_SIZE          =>     18;
use constant PASSWORD_HASH_SIZE         =>     64;
use constant PROGRESS_MAX_SIZE          =>      4;
use constant PGP_MAX_SIZE               => 148814;
use constant FIELD_SINGLE_LINE_MAX_SIZE =>     88;
use constant FIELD_MULTI_LINE_MAX_SIZE  =>   1488;

use constant PROGRESS_DIS               => 0;
use constant PROGRESS_PAGES             => 1;
use constant PROGRESS_LOC               => 2;

use constant PROGRESS_MAX               => 9999;

use constant PASSWORD_DEF               => 'libre';
use constant PASSWORD_SALT              => 'g%1AcHp1+RzF';

use constant LANG_DEF                   => 'ru';

use constant FORMAT_DIGITAL_EPUB        => 'Digital [EPUB]';
use constant FORMAT_DIGITAL_EPUB_RU     => 'Цифровой [EPUB]';
use constant FORMAT_DIGITAL_FB2         => 'Digital [FB2]';
use constant FORMAT_DIGITAL_FB2_RU      => 'Цифровой [FB2]';
use constant FORMAT_DIGITAL_AMAZON      => 'Digital [AZW/MOBI]';
use constant FORMAT_DIGITAL_AMAZON_RU   => 'Цифровой [AZW/MOBI]';
use constant FORMAT_DIGITAL_PDF         => 'Digital [PDF]';
use constant FORMAT_DIGITAL_PDF_RU      => 'Цифровой [PDF]';
use constant FORMAT_DIGITAL_HTML        => 'Digital [HTML]';
use constant FORMAT_DIGITAL_HTML_RU     => 'Цифровой [HTML]';
use constant FORMAT_DIGITAL_TXT         => 'Digital [TXT]';
use constant FORMAT_DIGITAL_TXT_RU      => 'Цифровой [TXT]';
use constant FORMAT_PHYSICAL            => 'Physical';
use constant FORMAT_PHYSICAL_RU         => 'Физический';

use constant CATEGORY_COLLECTION        => 'Collection';
use constant CATEGORY_COLLECTION_RU     => 'Коллекция';
use constant CATEGORY_WANTLIST          => 'Wantlist';
use constant CATEGORY_WANTLIST_RU       => 'Желания';

use constant RATING_MAX                 => 5;

use constant COMMENT_MAX_LINES          =>   9;
use constant ABOUT_MAX_LINES            =>  15;
use constant PGP_MAX_LINES              => 118;

################################################################################
# Database
################################################################################

sub filename_get
{
    my $username = shift;

    return DIR . "$username.sqlite3";
}

################################################################################

sub connect
{
    my $db;
    my $filename;

    my $username = shift;
    my $creation = shift;

    $filename = filename_get($username);

    if ($creation || -f $filename)
    {
        mkdir DIR if $creation;

        #DBI->trace(1, 'db/log');

        $db = DBI->connect("dbi:SQLite:dbname=$filename",
                           '',
                           '',
                           {'PrintWarn'  => 1,
                            'PrintError' => 1,
                            'RaiseError' => 0,
                            'AutoCommit' => 0});
    }

    return $db;
}

################################################################################

sub create
{
    my $db = shift;

    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS profile    (name           TEXT    NOT NULL,
                                       about          TEXT    NOT NULL,
                                       avatar         INTEGER NOT NULL -- 0 means title has no avatar, 1 — title has avatar
                                      )
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS settings   (password       TEXT    NOT NULL,
                                       lang           TEXT    NOT NULL,
                                       pgp            TEXT    NOT NULL)
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS authors    (author_id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       author         TEXT    NOT NULL UNIQUE)
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS titles     (title_id       INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       title          TEXT    NOT NULL,
                                       isbn           TEXT    NOT NULL,
                                       date           TEXT    NOT NULL,
                                       cover          INTEGER NOT NULL, -- 0 means title has no cover, 1 — title has cover
                                       rating         INTEGER NOT NULL, -- 0 means title hasn't been rated, 1 to RATING_MAX — it is
                                       progress       INTEGER NOT NULL, -- PROGRESS_DIS/PROGRESS_PAGES/PROGRESS_LOC
                                       progress_begin INTEGER,          -- NULL means progress column is PROGRESS_DIS
                                       progress_end   INTEGER,          -- NULL means progress column is PROGRESS_DIS
                                       comment        TEXT    NOT NULL,
                                       author_id      INTEGER NOT NULL,
                                       publisher_id   INTEGER NOT NULL,
                                       format_id      INTEGER NOT NULL,
                                       category_id    INTEGER NOT NULL,

                                      FOREIGN KEY(author_id)    REFERENCES authors   (author_id),
                                      FOREIGN KEY(publisher_id) REFERENCES publishers(publisher_id),
                                      FOREIGN KEY(format_id)    REFERENCES formats   (format_id),
                                      FOREIGN KEY(category_id)  REFERENCES categories(category_id))
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS publishers (publisher_id   INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       publisher      TEXT    NOT NULL UNIQUE)
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS formats    (format_id      INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       format         TEXT    NOT NULL UNIQUE,
                                       format_ru      TEXT    NOT NULL UNIQUE)
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS categories (category_id    INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
                                       category       TEXT    NOT NULL UNIQUE,
                                       category_ru    TEXT    NOT NULL UNIQUE)
;
    $db->do(<< ';');
CREATE TABLE IF NOT EXISTS Aliases    (author                TEXT NOT NULL,
                                       author_ru             TEXT NOT NULL,
                                       title                 TEXT NOT NULL,
                                       title_ru              TEXT NOT NULL,
                                       publisher             TEXT NOT NULL,
                                       publisher_ru          TEXT NOT NULL,
                                       isbn                  TEXT NOT NULL,
                                       isbn_ru               TEXT NOT NULL,
                                       format                TEXT NOT NULL,
                                       format_ru             TEXT NOT NULL,
                                       date                  TEXT NOT NULL,
                                       date_ru               TEXT NOT NULL,
                                       rating                TEXT NOT NULL,
                                       rating_ru             TEXT NOT NULL,
                                       category              TEXT NOT NULL,
                                       category_ru           TEXT NOT NULL,
                                       progress              TEXT NOT NULL,
                                       progress_ru           TEXT NOT NULL,
                                       comment               TEXT NOT NULL,
                                       comment_ru            TEXT NOT NULL,
                                       page_menu_products    TEXT NOT NULL,
                                       page_menu_products_ru TEXT NOT NULL,
                                       no_proposal           TEXT NOT NULL,
                                       no_proposal_ru        TEXT NOT NULL,
                                       pages                 TEXT NOT NULL,
                                       pages_ru              TEXT NOT NULL,
                                       locations             TEXT NOT NULL,
                                       locations_ru          TEXT NOT NULL)
;
    $db->do("CREATE TRIGGER IF NOT EXISTS profile_insert_integrity
BEFORE INSERT ON profile
WHEN (SELECT COUNT(*) FROM profile) = 1

     OR

     NEW.name LIKE '%' || x'0A' || '%' OR
     NEW.name LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.name) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     (LENGTH(NEW.about) - LENGTH(REPLACE(NEW.about, x'0A', ''))) / LENGTH(x'0A') > " . (ABOUT_MAX_LINES - 1) . " OR
     (LENGTH(NEW.about) - LENGTH(REPLACE(NEW.about, x'0D', ''))) / LENGTH(x'0D') > " . (ABOUT_MAX_LINES - 1) . ' OR
     LENGTH(NEW.about) > ' . FIELD_MULTI_LINE_MAX_SIZE . '

     OR

     NOT (NEW.avatar = 0 OR
          NEW.avatar = 1)
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS profile_update_integrity
BEFORE UPDATE ON profile
WHEN NEW.name LIKE '%' || x'0A' || '%' OR
     NEW.name LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.name) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     (LENGTH(NEW.about) - LENGTH(REPLACE(NEW.about, x'0A', ''))) / LENGTH(x'0A') > " . (ABOUT_MAX_LINES - 1) . " OR
     (LENGTH(NEW.about) - LENGTH(REPLACE(NEW.about, x'0D', ''))) / LENGTH(x'0D') > " . (ABOUT_MAX_LINES - 1) . ' OR
     LENGTH(NEW.about) > ' . FIELD_MULTI_LINE_MAX_SIZE . '

     OR

     NOT (NEW.avatar = 0 OR
          NEW.avatar = 1)
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do('CREATE TRIGGER IF NOT EXISTS settings_insert_integrity
BEFORE INSERT ON settings
WHEN (SELECT COUNT(*) FROM settings) = 1                OR
     LENGTH(NEW.password) != ' . PASSWORD_HASH_SIZE . " OR
     LENGTH(NEW.lang)     != 2

     OR

     (LENGTH(NEW.pgp) - LENGTH(REPLACE(NEW.pgp, x'0A', ''))) / LENGTH(x'0A') > " . (PGP_MAX_LINES - 1) . " OR
     (LENGTH(NEW.pgp) - LENGTH(REPLACE(NEW.pgp, x'0D', ''))) / LENGTH(x'0D') > " . (PGP_MAX_LINES - 1) . ' OR
     LENGTH(NEW.pgp) > ' . PGP_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do('CREATE TRIGGER IF NOT EXISTS settings_update_integrity
BEFORE UPDATE ON settings
WHEN LENGTH(NEW.password) != ' . PASSWORD_HASH_SIZE . " OR
     LENGTH(NEW.lang)     != 2

     OR

     (LENGTH(NEW.pgp) - LENGTH(REPLACE(NEW.pgp, x'0A', ''))) / LENGTH(x'0A') > " . (PGP_MAX_LINES - 1) . " OR
     (LENGTH(NEW.pgp) - LENGTH(REPLACE(NEW.pgp, x'0D', ''))) / LENGTH(x'0D') > " . (PGP_MAX_LINES - 1) . ' OR
     LENGTH(NEW.pgp) > ' . PGP_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS authors_insert_integrity
BEFORE INSERT ON authors
WHEN NEW.author = ''                     OR
     NEW.author LIKE '%' || x'0A' || '%' OR
     NEW.author LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.author) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS authors_update_integrity
BEFORE UPDATE ON authors
WHEN NEW.author = ''                     OR
     NEW.author LIKE '%' || x'0A' || '%' OR
     NEW.author LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.author) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do('CREATE TRIGGER IF NOT EXISTS titles_insert_integrity
BEFORE INSERT ON titles
WHEN (SELECT COUNT(*) FROM titles) = ' . TITLES_MAX . "

     OR

     NEW.title = ''                     OR
     NEW.title LIKE '%' || x'0A' || '%' OR
     NEW.title LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.title) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.isbn = ''                     OR
     NEW.isbn LIKE '%' || x'0A' || '%' OR
     NEW.isbn LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.isbn) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.date = ''                     OR
     NEW.date LIKE '%' || x'0A' || '%' OR
     NEW.date LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.date) > " . FIELD_SINGLE_LINE_MAX_SIZE . '

     OR

     NOT (NEW.cover = 0 OR
          NEW.cover = 1)

     OR

     NOT (NEW.rating >= 0 AND
          NEW.rating <= ' . RATING_MAX . ')

     OR

     ((NEW.progress_begin IS NOT NULL OR
       NEW.progress_end   IS NOT NULL)        AND NEW.progress  = ' . PROGRESS_DIS . ')

     OR

     ((NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_EPUB) . ') AND
       NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_FB2) . ') AND
       NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_AMAZON) . '))
                                              AND NEW.progress  = ' . PROGRESS_LOC . ')

     OR

     ((NEW.category_id = (SELECT category_id
                          FROM   categories
                          WHERE  category = ' . $db->quote(CATEGORY_WANTLIST) . ') OR
       NEW.progress_begin IS NULL OR
       NEW.progress_end   IS NULL OR
       NEW.progress_begin < 0     OR
       NEW.progress_end   < 0     OR
       NEW.progress_begin > ' . PROGRESS_MAX . ' OR
       NEW.progress_end   > ' . PROGRESS_MAX . ' OR
       NEW.progress_begin > NEW.progress_end) AND NEW.progress != ' . PROGRESS_DIS . ")

     OR

     (LENGTH(NEW.comment) - LENGTH(REPLACE(NEW.comment, x'0A', ''))) / LENGTH(x'0A') > " . (COMMENT_MAX_LINES - 1) . " OR
     (LENGTH(NEW.comment) - LENGTH(REPLACE(NEW.comment, x'0D', ''))) / LENGTH(x'0D') > " . (COMMENT_MAX_LINES - 1) . ' OR
     LENGTH(NEW.comment) > ' . FIELD_MULTI_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS titles_update_integrity
BEFORE UPDATE ON titles
WHEN NEW.title = ''                     OR
     NEW.title LIKE '%' || x'0A' || '%' OR
     NEW.title LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.title) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.isbn = ''                     OR
     NEW.isbn LIKE '%' || x'0A' || '%' OR
     NEW.isbn LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.isbn) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.date = ''                     OR
     NEW.date LIKE '%' || x'0A' || '%' OR
     NEW.date LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.date) > " . FIELD_SINGLE_LINE_MAX_SIZE . '

     OR

     NOT (NEW.cover = 0 OR
          NEW.cover = 1)

     OR

     NOT (NEW.rating >= 0 AND
          NEW.rating <= ' . RATING_MAX . ')

     OR

     ((NEW.progress_begin IS NOT NULL OR
       NEW.progress_end   IS NOT NULL)        AND NEW.progress  = ' . PROGRESS_DIS . ')

     OR

     ((NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_EPUB) . ') AND
       NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_FB2) . ') AND
       NEW.format_id   = (SELECT format_id
                          FROM   formats
                          WHERE  format != ' . $db->quote(FORMAT_DIGITAL_AMAZON) . '))
                                              AND NEW.progress  = ' . PROGRESS_LOC . ')

     OR

     ((NEW.category_id = (SELECT category_id
                          FROM   categories
                          WHERE  category = ' . $db->quote(CATEGORY_WANTLIST) . ') OR
       NEW.progress_begin IS NULL OR
       NEW.progress_end   IS NULL OR
       NEW.progress_begin < 0     OR
       NEW.progress_end   < 0     OR
       NEW.progress_begin > ' . PROGRESS_MAX . ' OR
       NEW.progress_end   > ' . PROGRESS_MAX . ' OR
       NEW.progress_begin > NEW.progress_end) AND NEW.progress != ' . PROGRESS_DIS . ")

     OR

     (LENGTH(NEW.comment) - LENGTH(REPLACE(NEW.comment, x'0A', ''))) / LENGTH(x'0A') > " . (COMMENT_MAX_LINES - 1) . " OR
     (LENGTH(NEW.comment) - LENGTH(REPLACE(NEW.comment, x'0D', ''))) / LENGTH(x'0D') > " . (COMMENT_MAX_LINES - 1) . ' OR
     LENGTH(NEW.comment) > ' . FIELD_MULTI_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS publishers_insert_integrity
BEFORE INSERT ON publishers
WHEN NEW.publisher = ''                     OR
     NEW.publisher LIKE '%' || x'0A' || '%' OR
     NEW.publisher LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.publisher) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS publishers_update_integrity
BEFORE UPDATE ON publishers
WHEN NEW.publisher = ''                     OR
     NEW.publisher LIKE '%' || x'0A' || '%' OR
     NEW.publisher LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.publisher) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS formats_insert_integrity
BEFORE INSERT ON formats
WHEN NEW.format = ''                     OR
     NEW.format LIKE '%' || x'0A' || '%' OR
     NEW.format LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.format) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.format_ru = ''                     OR
     NEW.format_ru LIKE '%' || x'0A' || '%' OR
     NEW.format_ru LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.format_ru) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS formats_update_integrity
BEFORE UPDATE ON formats
WHEN NEW.format = ''                     OR
     NEW.format LIKE '%' || x'0A' || '%' OR
     NEW.format LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.format) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.format_ru = ''                     OR
     NEW.format_ru LIKE '%' || x'0A' || '%' OR
     NEW.format_ru LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.format_ru) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS categories_insert_integrity
BEFORE INSERT ON categories
WHEN NEW.category = ''                     OR
     NEW.category LIKE '%' || x'0A' || '%' OR
     NEW.category LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.category) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.category_ru = ''                     OR
     NEW.category_ru LIKE '%' || x'0A' || '%' OR
     NEW.category_ru LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.category_ru) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
    $db->do("CREATE TRIGGER IF NOT EXISTS categories_update_integrity
BEFORE UPDATE ON categories
WHEN NEW.category = ''                     OR
     NEW.category LIKE '%' || x'0A' || '%' OR
     NEW.category LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.category) > " . FIELD_SINGLE_LINE_MAX_SIZE . "

     OR

     NEW.category_ru = ''                     OR
     NEW.category_ru LIKE '%' || x'0A' || '%' OR
     NEW.category_ru LIKE '%' || x'0D' || '%' OR
     LENGTH(NEW.category_ru) > " . FIELD_SINGLE_LINE_MAX_SIZE . '
BEGIN
    SELECT RAISE(IGNORE);
END
');
}

################################################################################

sub profile_insert
{
    my $db = shift;

    $db->do(<< ';');
INSERT OR IGNORE INTO profile
VALUES                ('', '', 0)
;
}

################################################################################

sub profile_update
{
    my $query;

    my $db     = shift;

    my $name   = shift;
    my $about  = shift;
    my $avatar = shift;

    $query = $db->prepare(<< ';');
UPDATE profile
SET    name   = ?,
       about  = ?,
       avatar = ?
;

    $query->bind_param(1, $$name,  DBI::SQL_VARCHAR);
    $query->bind_param(2, $$about, DBI::SQL_VARCHAR);
    $query->bind_param(3, $avatar, DBI::SQL_INTEGER);

    $query->execute();
    $query->finish();
}

################################################################################

sub settings_insert
{
    my $query;

    my $db       = shift;

    my $password = shift;
    my $lang     = shift;
    my $pgp      = shift;

    $query = $db->prepare(<< ';');
INSERT OR IGNORE INTO settings
VALUES                (?, ?, ?)
;

    $query->bind_param(1, Digest::SHA::sha256_hex($$password . PASSWORD_SALT), DBI::SQL_VARCHAR);
    $query->bind_param(2, $$lang,                                              DBI::SQL_VARCHAR);
    $query->bind_param(3, $$pgp,                                               DBI::SQL_VARCHAR);

    $query->execute();
    $query->finish();
}

################################################################################

sub settings_update
{
    my $query;

    my $db       = shift;

    my $password = shift;
    my $lang     = shift;
    my $pgp      = shift;

    if (defined $password)
    {
        $query = $db->prepare(<< ';');
UPDATE settings
SET    password = ?
;

        $query->bind_param(1, Digest::SHA::sha256_hex($$password . PASSWORD_SALT), DBI::SQL_VARCHAR);

        $query->execute();
        $query->finish();
    }

    if (defined $lang)
    {
        $query = $db->prepare(<< ';');
UPDATE settings
SET    lang     = ?
;

        $query->bind_param(1, $$lang, DBI::SQL_VARCHAR);
        $query->execute();
        $query->finish();
    }

    if (defined $pgp)
    {
        $query = $db->prepare(<< ';');
UPDATE settings
SET    pgp      = ?
;

        $query->bind_param(1, $$pgp, DBI::SQL_VARCHAR);
        $query->execute();
        $query->finish();
    }
}

################################################################################

sub insert
{
    tie my %formats,       'Tie::IxHash';
    tie my %formats_ru,    'Tie::IxHash';
    tie my %categories,    'Tie::IxHash';
    tie my %categories_ru, 'Tie::IxHash';

    my $db = shift;

    profile_insert($db);
    settings_insert($db, \PASSWORD_DEF, \LANG_DEF, \'');

    $formats      {db::FORMAT_DIGITAL_EPUB     } = FORMAT_DIGITAL_EPUB;
    $formats_ru   {db::FORMAT_DIGITAL_EPUB_RU  } = FORMAT_DIGITAL_EPUB_RU;
    $formats      {db::FORMAT_DIGITAL_FB2      } = FORMAT_DIGITAL_FB2;
    $formats_ru   {db::FORMAT_DIGITAL_FB2_RU   } = FORMAT_DIGITAL_FB2_RU;
    $formats      {db::FORMAT_DIGITAL_AMAZON   } = FORMAT_DIGITAL_AMAZON;
    $formats_ru   {db::FORMAT_DIGITAL_AMAZON_RU} = FORMAT_DIGITAL_AMAZON_RU;
    $formats      {db::FORMAT_DIGITAL_PDF      } = FORMAT_DIGITAL_PDF;
    $formats_ru   {db::FORMAT_DIGITAL_PDF_RU   } = FORMAT_DIGITAL_PDF_RU;
    $formats      {db::FORMAT_DIGITAL_HTML     } = FORMAT_DIGITAL_HTML;
    $formats_ru   {db::FORMAT_DIGITAL_HTML_RU  } = FORMAT_DIGITAL_HTML_RU;
    $formats      {db::FORMAT_DIGITAL_TXT      } = FORMAT_DIGITAL_TXT;
    $formats_ru   {db::FORMAT_DIGITAL_TXT_RU   } = FORMAT_DIGITAL_TXT_RU;
    $formats      {db::FORMAT_PHYSICAL         } = FORMAT_PHYSICAL;
    $formats_ru   {db::FORMAT_PHYSICAL_RU      } = FORMAT_PHYSICAL_RU;

    $categories   {db::CATEGORY_COLLECTION     } = CATEGORY_COLLECTION;
    $categories_ru{db::CATEGORY_COLLECTION_RU  } = CATEGORY_COLLECTION_RU;
    $categories   {db::CATEGORY_WANTLIST       } = CATEGORY_WANTLIST;
    $categories_ru{db::CATEGORY_WANTLIST_RU    } = CATEGORY_WANTLIST_RU;

    authors_publishers_formats_categories_insert($db,

                                                 undef,
                                                 undef,
                                                 \%formats,
                                                 \%formats_ru,
                                                 \%categories,
                                                 \%categories_ru);
}

################################################################################

sub authors_publishers_formats_categories_insert
{
    my $query;

    my $db            = shift;

    my $authors       = shift;
    my $publishers    = shift;
    my $formats       = shift;
    my $formats_ru    = shift;
    my $categories    = shift;
    my $categories_ru = shift;

    if (defined $authors)
    {
        my @authors;

        @authors = keys %$authors;

        $query = $db->prepare(<< ';');
INSERT OR IGNORE INTO authors    (author)
VALUES                           (?)
;

        $query->bind_param_array(1, \@authors, DBI::SQL_VARCHAR);
        $query->execute_array(undef);
        $query->finish();
    }

    if (defined $publishers)
    {
        my @publishers;

        @publishers = keys %$publishers;

        $query = $db->prepare(<< ';');
INSERT OR IGNORE INTO publishers (publisher)
VALUES                           (?)
;

        $query->bind_param_array(1, \@publishers, DBI::SQL_VARCHAR);
        $query->execute_array(undef);
        $query->finish();
    }

    if (defined $formats)
    {
        my @formats;
        my @formats_ru;

        @formats    = keys %$formats;
        @formats_ru = keys %$formats_ru;

        $query = $db->prepare(<< ';');
INSERT OR IGNORE INTO formats    (format, format_ru)
VALUES                           (?, ?)
;

        $query->bind_param_array(1, \@formats,    DBI::SQL_VARCHAR);
        $query->bind_param_array(2, \@formats_ru, DBI::SQL_VARCHAR);
        $query->execute_array(undef);
        $query->finish();
    }

    if (defined $categories)
    {
        my @categories;
        my @categories_ru;

        @categories    = keys %$categories;
        @categories_ru = keys %$categories_ru;

        $query = $db->prepare(<< ';');
INSERT OR IGNORE INTO categories (category, category_ru)
VALUES                           (?, ?)
;

        $query->bind_param_array(1, \@categories,    DBI::SQL_VARCHAR);
        $query->bind_param_array(2, \@categories_ru, DBI::SQL_VARCHAR);
        $query->execute_array(undef);
        $query->finish();
    }
}

################################################################################

sub authors_publishers_formats_categories_delete
{
    my $db = shift;

    $db->do(<< ';');
DELETE FROM authors
WHERE       author_id    NOT IN (SELECT author_id
                                 FROM   titles)
;
    $db->do(<< ';');
DELETE FROM publishers
WHERE       publisher_id NOT IN (SELECT publisher_id
                                 FROM   titles)
;
#    $db->do(<< ';');
#DELETE FROM formats
#WHERE       format_id    NOT IN (SELECT format_id
#                                 FROM   titles)
#;
#    $db->do(<< ';');
#DELETE FROM categories
#WHERE       category_id  NOT IN (SELECT category_id
#                                 FROM   titles)
#;
}

################################################################################

sub title_insert
{
    my $query;
    my $title_id;

    my $db             = shift;

    my $author         = shift;
    my $title          = shift;
    my $publisher      = shift;
    my $isbn           = shift;
    my $format         = shift;
    my $date           = shift;
    my $cover          = shift;
    my $rating         = shift;
    my $category       = shift;
    my $progress       = shift;
    my $progress_begin = shift;
    my $progress_end   = shift;
    my $comment        = shift;

    $query = $db->prepare(<< ';');
INSERT INTO titles(title, isbn, date, cover, rating,
                       progress, progress_begin, progress_end, comment,
                       author_id, publisher_id, format_id, category_id)
VALUES            (
                      ?,
                      ?,
                      ?,
                      ?,
                      ?,
                      ?,
                      ?,
                      ?,
                      ?,

                      (SELECT author_id
                       FROM   authors
                       WHERE  author    = ?),

                      (SELECT publisher_id
                       FROM   publishers
                       WHERE  publisher = ?),

                      (SELECT format_id
                       FROM   formats
                       WHERE  format    = ?),

                      (SELECT category_id
                       FROM   categories
                       WHERE  category  = ?)
                  )
;

    $query->bind_param( 1, $$title,          DBI::SQL_VARCHAR);
    $query->bind_param( 2, $$isbn,           DBI::SQL_VARCHAR);
    $query->bind_param( 3, $$date,           DBI::SQL_VARCHAR);
    $query->bind_param( 4,  $cover,          DBI::SQL_INTEGER);
    $query->bind_param( 5,  $rating,         DBI::SQL_INTEGER);
    $query->bind_param( 6,  $progress,       DBI::SQL_INTEGER);
    $query->bind_param( 7,  $progress_begin, DBI::SQL_INTEGER);
    $query->bind_param( 8,  $progress_end,   DBI::SQL_INTEGER);
    $query->bind_param( 9, $$comment,        DBI::SQL_VARCHAR);
    $query->bind_param(10, $$author,         DBI::SQL_VARCHAR);
    $query->bind_param(11, $$publisher,      DBI::SQL_VARCHAR);
    $query->bind_param(12, $$format,         DBI::SQL_VARCHAR);
    $query->bind_param(13, $$category,       DBI::SQL_VARCHAR);

    $query->execute();
    $query->finish();

    $title_id = $db->last_insert_id(undef, undef, 'titles', undef);

    return $title_id;
}

################################################################################

sub title_update
{
    my $query;

    my $db             = shift;

    my $author         = shift;
    my $title          = shift;
    my $publisher      = shift;
    my $isbn           = shift;
    my $format         = shift;
    my $date           = shift;
    my $cover          = shift;
    my $rating         = shift;
    my $category       = shift;
    my $progress       = shift;
    my $progress_begin = shift;
    my $progress_end   = shift;
    my $comment        = shift;

    my $title_id       = shift;

    if (defined $rating)
    {
        $query = $db->prepare(<< ';');
UPDATE titles
SET    rating = CASE WHEN rating = ?
                THEN 0
                ELSE ?
                END
WHERE  title_id = ?
;

        $query->bind_param(1, $rating,   DBI::SQL_INTEGER);
        $query->bind_param(2, $rating,   DBI::SQL_INTEGER);
        $query->bind_param(3, $title_id, DBI::SQL_INTEGER);
    }
    else
    {
        $query = $db->prepare(<< ';');
UPDATE titles
SET    title          = ?,
       isbn           = ?,
       date           = ?,
       cover          = ?,
       progress       = ?,
       progress_begin = ?,
       progress_end   = ?,
       comment        = ?,

       author_id      = (SELECT author_id
                         FROM   authors
                         WHERE  author    = ?),

       publisher_id   = (SELECT publisher_id
                         FROM   publishers
                         WHERE  publisher = ?),

       format_id      = (SELECT format_id
                         FROM   formats
                         WHERE  format    = ?),

       category_id    = (SELECT category_id
                         FROM   categories
                         WHERE  category  = ?)
WHERE  title_id = ?
;

        $query->bind_param( 1, $$title,          DBI::SQL_VARCHAR);
        $query->bind_param( 2, $$isbn,           DBI::SQL_VARCHAR);
        $query->bind_param( 3, $$date,           DBI::SQL_VARCHAR);
        $query->bind_param( 4,  $cover,          DBI::SQL_INTEGER);
        $query->bind_param( 5,  $progress,       DBI::SQL_INTEGER);
        $query->bind_param( 6,  $progress_begin, DBI::SQL_INTEGER);
        $query->bind_param( 7,  $progress_end,   DBI::SQL_INTEGER);
        $query->bind_param( 8, $$comment,        DBI::SQL_VARCHAR);
        $query->bind_param( 9, $$author,         DBI::SQL_VARCHAR);
        $query->bind_param(10, $$publisher,      DBI::SQL_VARCHAR);
        $query->bind_param(11, $$format,         DBI::SQL_VARCHAR);
        $query->bind_param(12, $$category,       DBI::SQL_VARCHAR);
        $query->bind_param(13,  $title_id,       DBI::SQL_INTEGER);
    }

    $query->execute();
    $query->finish();
}

################################################################################

sub disconnect
{
    my $db = shift;

    $db->disconnect();
}

return 1;
