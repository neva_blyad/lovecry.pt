=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
img.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare img package
package img;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
use Cwd;
use File::Temp;
use Image::Magick;
use Regexp::Common;
use Unicode::String;

# Constants:
#
#  - Image
use constant FONT_SIZE => 96;
use constant LETTER_X  => 8;
use constant LETTER_Y  => 8;

use constant TMP       => '/tmp/';
use constant BUF_SIZE  =>        1 * 1024;
use constant MAX_SIZE  => 5 * 1024 * 1024;

################################################################################

sub create
{
    my $err;

    my $img  = shift;

    my $str1 = shift;
    my $str2 = shift;
    my $bg   = shift;
    my $font = shift;

    $err = $img->Read('filename' => $$bg) and die $err;

    no warnings;
        $str1 = Unicode::String::utf8($$str1);
    if ($str1->length() > 0)
    {
        $err = $img->Annotate('text'      => $str1->substr(0, 1),
                              'font'      => $$font,
                              'pointsize' => FONT_SIZE,
                              'fill'      => 'black',
                              'gravity'   => 'NorthWest',
                              'x'         => LETTER_X,
                              'y'         => LETTER_Y - 24) and die $err;
    }

        $str2 = Unicode::String::utf8($$str2);
    if ($str2->length() > 0)
    {
        $err = $img->Annotate('text'      => $str2->substr(0, 1),
                              'font'      => $$font,
                              'pointsize' => FONT_SIZE,
                              'fill'      => 'black',
                              'gravity'   => 'SouthEast',
                              'x'         => LETTER_X,
                              'y'         => LETTER_Y - 24) and die $err;
    }
    use warnings;

    return 1;
}

################################################################################

sub resize
{
    my $err;

    my ($width, $height);

    my $img        = shift;

    my $filename   = shift;

    my $max_width  = shift;
    my $max_height = shift;

        $err =  $img->Read('filename' => $$filename);
    if ($err =~ /$RE{'num'}{'int'}/)
    {
        return 0 if $& == Image::Magick->DelegateWarning        ||
                    $& == Image::Magick->CorruptImageWarning    ||
                    $& == Image::Magick->MissingDelegateWarning ||
                    $& == Image::Magick->ImageWarning;
        return 0 if $& == Image::Magick->DelegateError          ||
                    $& == Image::Magick->MissingDelegateError   ||
                    $& == Image::Magick->CorruptImageError      ||
                    $& == Image::Magick->ImageError;
        die $err;
    }

    $width = $img->Get('width');

    if ($width > $max_width)
    {
        $width = $max_width;
        $err = $img->Resize('geometry' => $width) and die $err;
    }

    $height = $img->Get('height');

    if ($height > $max_height)
    {
        $height = $max_height;
        $err = $img->Resize('geometry' => "x$height") and die $err;
    }

    return 1;
}

################################################################################

sub dl
{
    my $tmp;
    my $len;

    my $desc = shift;

    $tmp = File::Temp->new('TEMPLATE' => TMP . 'imgXXXX',
                           'UNLINK'   => 0);

    while (read $desc, my $buf, BUF_SIZE)
    {
        print $tmp $buf;

                $len += BUF_SIZE;
        last if $len >= MAX_SIZE;
    }

    close $tmp;

    if ($len >= MAX_SIZE)
    {
        File::Temp::cleanup();

        $tmp = undef;
    }
    else
    {
        $tmp = Cwd::abs_path($tmp);
    }

    return $tmp;
}

################################################################################

sub dl_complete
{
    File::Temp::cleanup();
}

################################################################################

sub make
{
    my $res;

    my $img        = shift;

    my $filename   = shift;

    my $str1       = shift;
    my $str2       = shift;
    my $bg         = shift;
    my $font       = shift;

    my $max_width  = shift;
    my $max_height = shift;

    $$img = new Image::Magick() unless defined $$img;

    if (defined $$filename)
    {
        $res = resize($$img,

                      $filename,

                      $max_width,
                      $max_height);
    }
    else
    {
        $res = create($$img,

                      $str1,
                      $str2,
                      $bg,
                      $font);
    }

    return $res;
}


################################################################################

sub make_complete
{
    my $err;

    my $img      = shift;

    my $filename = shift;

    $err = $img->Write('filename' => "$$filename.png") and die $err;
}

return 1;
