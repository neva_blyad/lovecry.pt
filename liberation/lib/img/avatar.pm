=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
avatar.pm

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# Declare avatar package
package img::avatar;

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# CPAN modules
#use Cwd;
use File::Basename;
use File::Spec;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../";

# Our own modules
use img;

# Constants:
#
#  - Cover
use constant DIR        => 'avatar/';

use constant BG         => 'template.$$$.png';
use constant FONT       => 'template.ttf';

use constant MAX_WIDTH  => 200;
use constant MAX_HEIGHT => 200;

################################################################################
# Avatar
################################################################################

sub init
{
    my $img;

    my $username = shift;
    my $name     = shift;

    mkdir DIR;
    dir_change();

    unless (-f "$username.png")
    {
        make(\$img,
             \undef,
             \'',
             \'');
        make_complete($img, \$username);
    }

    dir_back_change();
}

################################################################################

sub dir_change
{
    chdir DIR;
}

################################################################################

sub dir_back_change
{
    chdir '../';
}

################################################################################

sub dl
{
    return img::dl(@_);
}

################################################################################

sub dl_complete
{
    img::dl_complete();
}

################################################################################

sub make
{
    img::make(@_,
              \BG,
              \FONT,

              MAX_WIDTH,
              MAX_HEIGHT);
}

################################################################################

sub make_complete
{
    img::make_complete(@_);
}

return 1;
