#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
title.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
#use Cwd;
use DBI;
use Encode;
use File::Basename;
use File::Spec;
use File::stat;
use HTML::Entities;
use Locale::gettext;
use Regexp::Common;
use Try::Tiny;
use Unicode::String;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../lib/";
use lib "$path/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;
use img;
use img::cover;

# Constants:
#
#  - CGI,
use constant CGI_ACTION_VIEW                    => 0;
use constant CGI_ACTION_EDIT                    => 1;
use constant CGI_ACTION_ADD                     => 2;
use constant CGI_ACTION_RATE                    => 3;

#  - Database
use constant DB_SELECT_LANG                     => 0;
use constant DB_SELECT_TITLES_CNT               => 1;
use constant DB_SELECT_TITLE                    => 2;
use constant DB_SELECT_TITLE_COVER              => 3;
use constant DB_SELECT_FORMATS_CATEGORIES       => 4;
use constant DB_SELECT_AUTHORS_PUBLISHERS       => 5;
use constant DB_SELECT_ALIASES                  => 6;

use constant DB_INSERT                          => 0;
use constant DB_INSERT_AUTHORS_PUBLISHERS_TITLE => 1;

use constant DB_UPDATE_TITLE                    => 0;
use constant DB_UPDATE_TITLE_RATING             => 1;

use constant DB_RES_NONE                        => 0b0000000000;
use constant DB_RES_SUCCESS                     => 0b0000000001;
use constant DB_RES_ERR_AUTHOR                  => 0b0000000010;
use constant DB_RES_ERR_TITLE                   => 0b0000000100;
use constant DB_RES_ERR_PUBLISHER               => 0b0000001000;
use constant DB_RES_ERR_ISBN                    => 0b0000010000;
use constant DB_RES_ERR_DATE                    => 0b0000100000;
use constant DB_RES_ERR_PROGRESS                => 0b0001000000;
use constant DB_RES_ERR_COMMENT                 => 0b0010000000;
use constant DB_RES_ERR_COVER_DL                => 0b0100000000;
use constant DB_RES_ERR_COVER_BAD               => 0b1000000000;

# Global Variables:
#
#  - CGI,
our $cgi                = undef;
our $cgi_space          = undef;
our $cgi_seSSion        = undef;
our $cgi_auth           = undef;
our $cgi_auth_ok        = undef;

our %cgi_username       = ();
our %cgi_title_id       = ();
our %cgi_action         = ();

our %cgi_author         = ();
our %cgi_title          = ();
our %cgi_publisher      = ();
our %cgi_isbn           = ();
our %cgi_format         = ();
our %cgi_date           = ();
our %cgi_category       = ();
our %cgi_progress       = ();
our %cgi_progress_begin = ();
our %cgi_progress_end   = ();
our %cgi_comment        = ();
our %cgi_cover          = ();
our %cgi_cover_del      = ();

our %cgi_rating         = ();

#  - Database
our $db                 = undef;

our $db_locale          = undef;

our $db_titles_cnt      = undef;

our $db_authors         = undef;
our $db_publishers      = undef;
our $db_formats         = undef;
our $db_categories      = undef;
our @db_Aliases         = ();

our $db_author_Alias    = undef;
our $db_title_Alias     = undef;
our $db_publisher_Alias = undef;
our $db_isbn_Alias      = undef;
our $db_format_Alias    = undef;
our $db_date_Alias      = undef;
our $db_category_Alias  = undef;
our $db_progress_Alias  = undef;
our $db_comment_Alias   = undef;

our $db_title_id        = undef;
our $db_title           = undef;
our $db_format          = undef;
our $db_category        = undef;
our $db_cover           = undef;

our $db_tmp             = undef;
our $db_img             = undef;

our $db_res             = undef;

#  - i18n
our $i18n               = undef;
our $i18n_space         = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    my $cgi;

    $cgi       = cgi::init();
    $cgi_space = cgi::space_get();

    $cgi_username      {'val'         } = $cgi->url_param('username');
    $cgi_username      {'verified_val'} = defined $cgi_username{'val'} && Unicode::String::utf8($cgi_username{'val'})->length() <= db::USERNAME_MAX_SIZE &&
                                                                          $cgi_username{'val'} =~ /^[a-z0-9_]+\z/ ?
                                              $cgi_username{'val'} : undef;

    $cgi_title_id      {'val'         } = $cgi->url_param('title_id');
    $cgi_title_id      {'verified_val'} = defined $cgi_title_id{'val'} && $cgi_title_id{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_title_id{'val'} > 0 ?
                                              $cgi_title_id{'val'} : undef;

    $cgi_action        {'val'         } = $cgi->url_param('action');
    $cgi_action        {'verified_val'} = defined $cgi_action{'val'} && grep({ $cgi_action{'val'} eq $_ } ('add',
                                                                                                           'edit',
                                                                                                           'rate')) ?
                                              $cgi_action{'val'} : undef;

    $cgi_cover         {'val'         } = $cgi->param('cover');
    $cgi_cover         {'verified_val'} = defined $cgi_cover{'val'} && $cgi_cover{'val'} ne '' ?
                                              $cgi_cover{'val'} : undef;

    $cgi_cover_del     {'val'         } = $cgi->param('cover_del');
    $cgi_cover_del     {'verified_val'} = defined $cgi_cover_del{'val'} && $cgi_cover_del{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_cover_del{'val'} == 1 ?
                                              $cgi_cover_del{'val'} : undef;

    $cgi_rating        {'val'         } = $cgi->url_param('rating');
    $cgi_rating        {'verified_val'} = defined $cgi_rating{'val'} && $cgi_rating{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_rating{'val'} >= 1 &&
                                                                                                                        $cgi_rating{'val'} <= db::RATING_MAX ?
                                              $cgi_rating{'val'} : undef;

    return $cgi;
}

################################################################################

sub cgi_postinit
{
    $cgi_author        {'val'         } = $cgi->param($db_author_Alias);
    $cgi_author        {'verified_val'} = defined $cgi_author{'val'} && $cgi_author{'val'} ne '' &&
                                                                        Unicode::String::utf8($cgi_author{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                        $cgi_author{'val'} !~ /\n|\r/ ?
                                              $cgi_author{'val'} : undef;

    $cgi_title         {'val'         } = $cgi->param($db_title_Alias);
    $cgi_title         {'verified_val'} = defined $cgi_title{'val'} && $cgi_title{'val'} ne '' &&
                                                                       Unicode::String::utf8($cgi_title{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                       $cgi_title{'val'} !~ /\n|\r/ ?
                                              $cgi_title{'val'} : undef;

    $cgi_publisher     {'val'         } = $cgi->param($db_publisher_Alias);
    $cgi_publisher     {'verified_val'} = defined $cgi_publisher{'val'} && $cgi_publisher{'val'} ne '' &&
                                                                           Unicode::String::utf8($cgi_publisher{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                           $cgi_publisher{'val'} !~ /\n|\r/ ?
                                              $cgi_publisher{'val'} : undef;

    $cgi_isbn          {'val'         } = $cgi->param($db_isbn_Alias);
    $cgi_isbn          {'verified_val'} = defined $cgi_isbn{'val'} && $cgi_isbn{'val'} ne '' &&
                                                                      Unicode::String::utf8($cgi_isbn{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                      $cgi_isbn{'val'} !~ /\n|\r/ ?
                                              $cgi_isbn{'val'} : undef;

    $cgi_format        {'val'         } = $cgi->param($db_format_Alias);
    $cgi_format        {'verified_val'} = defined $cgi_format{'val'} && $cgi_format{'val'} ne '' &&
                                                                        Unicode::String::utf8($cgi_format{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                        $cgi_format{'val'} !~ /\n|\r/ ?
                                              $cgi_format{'val'} : undef;

    $cgi_date          {'val'         } = $cgi->param($db_date_Alias);
    $cgi_date          {'verified_val'} = defined $cgi_date{'val'} && $cgi_date{'val'} ne '' &&
                                                                      Unicode::String::utf8($cgi_date{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                      $cgi_date{'val'} !~ /\n|\r/ ?
                                              $cgi_date{'val'} : undef;

    $cgi_category      {'val'         } = $cgi->param($db_category_Alias);
    $cgi_category      {'verified_val'} = defined $cgi_category{'val'} && $cgi_category{'val'} ne '' &&
                                                                          Unicode::String::utf8($cgi_category{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                          $cgi_category{'val'} !~ /\n|\r/ ?
                                              $cgi_category{'val'} : undef;

    $cgi_progress      {'val'         } = $cgi->param($db_progress_Alias);
    $cgi_progress      {'verified_val'} = defined $cgi_progress{'val'} && grep({ $cgi_progress{'val'} eq $_ } (db::PROGRESS_DIS,
                                                                                                               db::PROGRESS_PAGES,
                                                                                                               db::PROGRESS_LOC)) ?
                                              $cgi_progress{'val'} : undef;

    $cgi_progress_begin{'val'         } = $cgi->param("${db_progress_Alias}_begin");
    $cgi_progress_begin{'verified_val'} = defined $cgi_progress_begin{'val'} && ( $cgi_progress_begin{'val'} eq '' ||
                                                                                 ($cgi_progress_begin{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_progress_begin{'val'} >= 0 &&
                                                                                                                                          $cgi_progress_begin{'val'} <= db::PROGRESS_MAX)) ?
                                              $cgi_progress_begin{'val'} : undef;

    $cgi_progress_end  {'val'         } = $cgi->param("${db_progress_Alias}_end");
    $cgi_progress_end  {'verified_val'} = defined $cgi_progress_end{'val'} && ( $cgi_progress_end{'val'} eq '' ||
                                                                               ($cgi_progress_end{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_progress_end{'val'} >= 0 &&
                                                                                                                                      $cgi_progress_end{'val'} <= db::PROGRESS_MAX)) ?
                                              $cgi_progress_end{'val'} : undef;

    $cgi_comment       {'val'         } = $cgi->param($db_comment_Alias);
    $cgi_comment       {'verified_val'} = defined $cgi_comment{'val'} && (() = $cgi_comment{'val'} =~ /\n/g) <= db::COMMENT_MAX_LINES - 1 &&
                                                                         (() = $cgi_comment{'val'} =~ /\r/g) <= db::COMMENT_MAX_LINES - 1 &&
                                                                         Unicode::String::utf8($cgi_comment{'val'})->length() < db::FIELD_MULTI_LINE_MAX_SIZE ?
                                              $cgi_comment{'val'} : undef;
}

################################################################################

sub cgi_action_set
{
    my $cgi = shift;

    if (defined $cgi_action{'verified_val'})
    {
        if    ($cgi_action{'verified_val'} eq 'edit') { $cgi_action{'final_val'} = CGI_ACTION_EDIT; }
        elsif ($cgi_action{'verified_val'} eq 'add' ) { $cgi_action{'final_val'} = CGI_ACTION_ADD;  }
        else                                          { $cgi_action{'final_val'} = CGI_ACTION_RATE; }
    }
    else                                              { $cgi_action{'final_val'} = CGI_ACTION_VIEW; }
}

################################################################################

sub cgi_progress_set
{
    my $cgi = shift;

    if (defined $cgi_progress{'verified_val'}
        &&
        $cgi_progress{'verified_val'} == db::PROGRESS_DIS && $cgi_progress_begin{'verified_val'} eq '' &&
                                                             $cgi_progress_end  {'verified_val'} eq '')
    {
        $cgi_progress      {'final_val'} = $cgi_progress{'verified_val'};
        $cgi_progress_begin{'final_val'} = undef;
        $cgi_progress_end  {'final_val'} = undef;
    }
    elsif (defined $cgi_progress{'verified_val'}
           &&
           $cgi_progress{'verified_val'} != db::PROGRESS_DIS && $cgi_progress_begin{'verified_val'} =~ /^$RE{'num'}{'int'}\z/ &&
                                                                $cgi_progress_end  {'verified_val'} =~ /^$RE{'num'}{'int'}\z/ &&
                                                                    $cgi_progress_end{'verified_val'} >= $cgi_progress_begin{'verified_val'}
           &&
           !($cgi_progress{'verified_val'} == db::PROGRESS_LOC &&
             $cgi_format{'verified_val'}   ne db::FORMAT_DIGITAL_EPUB &&
             $cgi_format{'verified_val'}   ne db::FORMAT_DIGITAL_FB2  &&
             $cgi_format{'verified_val'}   ne db::FORMAT_DIGITAL_AMAZON)
           &&
           $cgi_category{'verified_val'} ne db::CATEGORY_WANTLIST)
    {
        $cgi_progress      {'final_val'} = $cgi_progress      {'verified_val'};
        $cgi_progress_begin{'final_val'} = $cgi_progress_begin{'verified_val'};
        $cgi_progress_end  {'final_val'} = $cgi_progress_end  {'verified_val'};
    }
    else
    {
        $cgi_progress      {'final_val'} = undef;
        $cgi_progress_begin{'final_val'} = undef;
        $cgi_progress_end  {'final_val'} = undef;
    }
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_postinit
{
    if (@db_Aliases > 0)
    {
        foreach my $idx (0 .. 8)
        {
            Encode::_utf8_on($db_Aliases[$idx]);
        }

        $db_author_Alias    = lc encode_entities($db_Aliases[0], '<>&"');
        $db_title_Alias     = lc encode_entities($db_Aliases[1], '<>&"');
        $db_publisher_Alias = lc encode_entities($db_Aliases[2], '<>&"');
        $db_isbn_Alias      = lc encode_entities($db_Aliases[3], '<>&"');
        $db_format_Alias    = lc encode_entities($db_Aliases[4], '<>&"');
        $db_date_Alias      = lc encode_entities($db_Aliases[5], '<>&"');
        $db_category_Alias  = lc encode_entities($db_Aliases[6], '<>&"');
        $db_progress_Alias  = lc encode_entities($db_Aliases[7], '<>&"');
        $db_comment_Alias   = lc encode_entities($db_Aliases[8], '<>&"');
    }
    else
    {
        $db_author_Alias    = 'author';
        $db_title_Alias     = 'title';
        $db_publisher_Alias = 'publisher';
        $db_isbn_Alias      = 'isbn';
        $db_format_Alias    = 'format';
        $db_date_Alias      = 'date';
        $db_category_Alias  = 'category';
        $db_progress_Alias  = 'progress';
        $db_comment_Alias   = 'comment';
    }

    $db_res = DB_RES_NONE;
}

################################################################################

sub db_connect
{
    my $db;

    return undef unless defined $cgi_username{'verified_val'};

    $db = db::connect($cgi_username{'verified_val'},
                      0 # Do not create database if it doesn't exist
                     );
    img::cover::init($cgi_username{'verified_val'}) if defined $db;

    return $db;
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_select
{
    my $query;

    my $db     = shift;
    my $action = shift;

    if ($action == DB_SELECT_TITLES_CNT ||
        $action == DB_SELECT_TITLE_COVER)
    {
        return unless (!defined $cgi_title_id{'verified_val'} && $action == DB_SELECT_TITLES_CNT) ||
                      ( defined $cgi_title_id{'verified_val'} && $action == DB_SELECT_TITLE_COVER);

        return unless defined $cgi_author        {'val'} ||
                      defined $cgi_title         {'val'} ||
                      defined $cgi_publisher     {'val'} ||
                      defined $cgi_isbn          {'val'} ||
                      defined $cgi_format        {'val'} ||
                      defined $cgi_date          {'val'} ||
                      defined $cgi_category      {'val'} ||
                      defined $cgi_progress      {'val'} ||
                      defined $cgi_progress_begin{'val'} ||
                      defined $cgi_progress_end  {'val'} ||
                      defined $cgi_comment       {'val'} ||
                      defined $cgi_cover         {'val'} ||
                      defined $cgi_cover_del     {'val'};

        $db_res |= DB_RES_ERR_AUTHOR    unless defined $cgi_author   {'verified_val'};
        $db_res |= DB_RES_ERR_TITLE     unless defined $cgi_title    {'verified_val'};
        $db_res |= DB_RES_ERR_PUBLISHER unless defined $cgi_publisher{'verified_val'};
        $db_res |= DB_RES_ERR_ISBN      unless defined $cgi_isbn     {'verified_val'};
        return                          unless defined $cgi_format   {'verified_val'};
        $db_res |= DB_RES_ERR_DATE      unless defined $cgi_date     {'verified_val'};
        return                          unless defined $cgi_category {'verified_val'};
        $db_res |= DB_RES_ERR_PROGRESS  unless defined $cgi_progress {'final_val'   };
        $db_res |= DB_RES_ERR_COMMENT   unless defined $cgi_comment  {'verified_val'};

        return if $db_res != DB_RES_NONE;

        return if defined $cgi_cover{'verified_val'} && defined $cgi_cover_del{'verified_val'};

        return if defined $cgi_rating{'val'};
    }

    if ($action == DB_SELECT_LANG)
    {
        $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
    }
    elsif ($action == DB_SELECT_TITLES_CNT)
    {
        $db_titles_cnt = ($db->selectrow_array('SELECT COUNT(*) FROM titles'))[0];
    }
    elsif ($action == DB_SELECT_TITLE)
    {
        my $i18n;

        return unless defined $cgi_title_id{'verified_val'};

        return if (defined $cgi_author        {'val'} ||
                   defined $cgi_title         {'val'} ||
                   defined $cgi_publisher     {'val'} ||
                   defined $cgi_isbn          {'val'} ||
                   defined $cgi_format        {'val'} ||
                   defined $cgi_date          {'val'} ||
                   defined $cgi_category      {'val'} ||
                   defined $cgi_progress      {'val'} ||
                   defined $cgi_progress_begin{'val'} ||
                   defined $cgi_progress_end  {'val'} ||
                   defined $cgi_comment       {'val'} ||
                   defined $cgi_cover         {'val'} ||
                   defined $cgi_cover_del     {'val'}) && $cgi_action{'final_val'} == CGI_ACTION_VIEW;

        return if defined $cgi_rating{'val'};

        return if $db_res == DB_RES_SUCCESS;

        $i18n = i18n::get();
        $i18n = ($i18n eq 'C') ? '' :
                                 '_' . substr $i18n, 0, 2;

        $query = $db->prepare(<< ";");
SELECT author, title, publisher, isbn,
           format, format$i18n, date, rating,
           category, category$i18n, progress, progress_begin,
           progress_end, comment, cover
FROM   titles

JOIN   authors
ON     titles.author_id    = authors.author_id
JOIN   publishers
ON     titles.publisher_id = publishers.publisher_id
JOIN   formats
ON     titles.format_id    = formats.format_id
JOIN   categories
ON     titles.category_id  = categories.category_id

WHERE  title_id = ?
;
#USING (author_id)
#USING (publisher_id)
#USING (format_id)
#USING (category_id)

                    $query->bind_param(1, $cgi_title_id{'verified_val'}, DBI::SQL_INTEGER);
                    $query->execute();
        $db_title = $query->fetch();
    }
    elsif ($action == DB_SELECT_TITLE_COVER)
    {
        $query = $db->prepare(<< ';');
SELECT cover
FROM   titles
WHERE  title_id = ?
;

        $query->bind_param(1, $cgi_title_id{'verified_val'}, DBI::SQL_INTEGER);
        $query->execute();
        $query->bind_col(1, \$db_cover);
        $query->fetchrow();
    }
    elsif ($action == DB_SELECT_FORMATS_CATEGORIES)
    {
        my $i18n;

        $i18n = i18n::get();
        $i18n = ($i18n eq 'C') ? '' :
                                 '_' . substr $i18n, 0, 2;

        $db_formats = $db->selectall_arrayref(<< ";");
SELECT format, format$i18n
FROM   formats
;

        if (defined $cgi_format{'verified_val'})
        {
            foreach my $row (@$db_formats)
            {
                my  $format;
                    $format = @$row[0];
                if ($format eq $cgi_format{'verified_val'})
                {
                    $db_format = $format;
                    last;
                }
            }
        }

        $db_categories = $db->selectall_arrayref(<< ";");
SELECT category, category$i18n
FROM   categories
;

        if (defined $cgi_category{'verified_val'})
        {
            foreach my $row (@$db_categories)
            {
                my  $category;
                    $category = @$row[0];
                if ($category eq $cgi_category{'verified_val'})
                {
                    $db_category = $category;
                    last;
                }
            }
        }
    }
    elsif ($action == DB_SELECT_AUTHORS_PUBLISHERS)
    {
        return if $db_res == DB_RES_SUCCESS;

        $db_authors    = $db->selectcol_arrayref(<< ';');
SELECT author
FROM   authors
;
        $db_publishers = $db->selectcol_arrayref(<< ';');
SELECT publisher
FROM   publishers
;
    }
    #elsif ($action == DB_SELECT_ALIASES)
    else
    {
        $db->{'PrintWarn'}  = 0;
        $db->{'PrintError'} = 0;
        @db_Aliases = $db->selectrow_array(<< ";");
SELECT author, title, publisher, isbn,
           format, date, category, progress,
           comment, pages, locations,
       author$i18n, title$i18n, publisher$i18n, isbn$i18n,
           format$i18n, date$i18n, category$i18n, progress$i18n,
           comment$i18n, pages$i18n, locations$i18n
FROM   Aliases
;
        $db->{'PrintWarn'}  = 1;
        $db->{'PrintError'} = 1;
    }
}

################################################################################

sub db_insert
{
    my $db     = shift;
    my $action = shift;

    if ($action == DB_INSERT)
    {
        db::insert($db);
    }
    #if ($action == DB_INSERT_AUTHORS_PUBLISHERS_TITLE)
    else
    {
        my %authors;
        my %publishers;

        return if ((!defined $db_titles_cnt || $db_titles_cnt == db::TITLES_MAX) && $cgi_action{'final_val'} == CGI_ACTION_ADD) ||
                  ( !defined $db_cover                                           && $cgi_action{'final_val'} == CGI_ACTION_EDIT);

        return unless defined $db_format &&
                      defined $db_category;

        if (defined $cgi_cover{'verified_val'})
        {
                            $db_tmp = img::cover::dl($cgi_cover{'verified_val'});
            unless (defined $db_tmp)
            {
                $db_res |= DB_RES_ERR_COVER_DL;
                return;
            }
        }

        if (defined $cgi_cover{'verified_val'} || defined $cgi_cover_del{'verified_val'} || !(defined $db_cover &&
                                                                                                      $db_cover == 1))
        {
            img::cover::dir_change($cgi_username{'verified_val'});

            if (!img::cover::make(\$db_img,

                                  \$db_tmp,

                                  \$cgi_author{'verified_val'},
                                  \$cgi_title {'verified_val'}))
            {
                $db_res |= DB_RES_ERR_COVER_BAD;
                $db_tmp  = undef;

                img::cover::dir_back_change();
                img::cover::dl_complete();
                return;
            }

            img::cover::dl_complete() if defined $cgi_cover{'verified_val'};
        }

        $authors   {$cgi_author   {'verified_val'}} = $cgi_author   {'verified_val'};
        $publishers{$cgi_publisher{'verified_val'}} = $cgi_publisher{'verified_val'};

        db::authors_publishers_formats_categories_insert($db,

                                                         \%authors,
                                                         \%publishers,
                                                         undef,
                                                         undef,
                                                         undef,
                                                         undef);

        if ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
        {
            img::cover::dir_back_change();
            return;
        }

        $db_title_id = db::title_insert($db,

                                        \$cgi_author       {'verified_val'},
                                        \$cgi_title        {'verified_val'},
                                        \$cgi_publisher    {'verified_val'},
                                        \$cgi_isbn         {'verified_val'},
                                        \$db_format,
                                        \$cgi_date         {'verified_val'},
                                        defined $cgi_cover {'verified_val'} ? 1 : 0,
                                        0, # Rating
                                        \$db_category,
                                        $cgi_progress      {'final_val'   },
                                        $cgi_progress_begin{'final_val'   },
                                        $cgi_progress_end  {'final_val'   },
                                        \$cgi_comment      {'verified_val'}
                                       );
        $db_res = DB_RES_SUCCESS;

        img::cover::make_complete($db_img, \$db_title_id);
        img::cover::dir_back_change();
    }
}

################################################################################

sub db_update
{
    my %authors;
    my %publishers;

    my $db     = shift;
    my $action = shift;

    if ($action == DB_UPDATE_TITLE)
    {
        return unless  defined $db_format   &&
                       defined $db_category &&
                       defined $db_cover    &&
                      (defined $db_tmp      || !defined $cgi_cover{'verified_val'});

        db::title_update($db,

                         \$cgi_author   {'verified_val'},
                         \$cgi_title    {'verified_val'},
                         \$cgi_publisher{'verified_val'},
                         \$cgi_isbn     {'verified_val'},
                         \$db_format,
                         \$cgi_date     {'verified_val'},
                         (defined $cgi_cover{'verified_val'} || (!defined $cgi_cover_del{'verified_val'} && defined $db_cover &&
                                                                                                                    $db_cover == 1)) ? 1 : 0,
                         undef,
                         \$db_category,
                         $cgi_progress      {'final_val'},
                         $cgi_progress_begin{'final_val'},
                         $cgi_progress_end  {'final_val'},
                         \$cgi_comment      {'verified_val'},

                         $cgi_title_id{'verified_val'});

        $db_title_id = $cgi_title_id{'verified_val'};
        $db_res      = DB_RES_SUCCESS;

        if (defined $cgi_cover{'verified_val'} || defined $cgi_cover_del{'verified_val'} || !(defined $db_cover &&
                                                                                                      $db_cover == 1))
        {
            img::cover::dir_change($cgi_username{'verified_val'});
            img::cover::make_complete($db_img, \$db_title_id);
            img::cover::dir_back_change();
        }
    }
    #elsif ($action == DB_UPDATE_TITLE_RATING)
    else
    {
        return unless defined $cgi_title_id{'verified_val'};

        return if defined $cgi_author        {'val'} ||
                  defined $cgi_title         {'val'} ||
                  defined $cgi_publisher     {'val'} ||
                  defined $cgi_isbn          {'val'} ||
                  defined $cgi_format        {'val'} ||
                  defined $cgi_date          {'val'} ||
                  defined $cgi_category      {'val'} ||
                  defined $cgi_progress      {'val'} ||
                  defined $cgi_progress_begin{'val'} ||
                  defined $cgi_progress_end  {'val'} ||
                  defined $cgi_comment       {'val'} ||
                  defined $cgi_cover         {'val'} ||
                  defined $cgi_cover_del     {'val'};

        return unless defined $cgi_rating{'verified_val'};

        db::title_update($db,

                         undef,
                         undef,
                         undef,
                         undef,
                         undef,
                         undef,
                         undef,
                         $cgi_rating{'verified_val'},
                         undef,
                         undef,
                         undef,
                         undef,
                         undef,

                         $cgi_title_id{'verified_val'});

        $db_title_id = $cgi_title_id{'verified_val'};
        $db_res      = DB_RES_SUCCESS;
    }
}

################################################################################

sub db_delete
{
    my $db = shift;

    return unless defined $db_title_id;

    db::authors_publishers_formats_categories_delete($db);
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# i18n
################################################################################

sub i18n_init
{
    i18n::init(\$db_locale, $cgi);

    $i18n = i18n::get();
    $i18n = $i18n eq 'C' ? '' :
                           '_' . substr $i18n, 0, 2;
}

################################################################################

sub i18n_space_set
{
    Encode::_utf8_on($cgi_space);
    $i18n_space = gettext(ucfirst $cgi_space);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init      ();
                                cgi_action_set($cgi);
    ($cgi_seSSion, $cgi_auth) = cgi_auth      ($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/../$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect($cgi_auth->{'profile'}{'username'});

        if (defined $db)
        {
            #db_create($db);
            #db_insert($db, DB_INSERT);
            db_select($db, DB_SELECT_LANG);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir "$path/../";

    i18n_init();
    i18n_space_set();
}
catch
{
    die "[2]: $_";
};

# Database.
#
# Get Aliases of the viewed profile.
try
{
    chdir $path;

    $db = db_connect();

    if (defined $db)
    {
        #db_create($db);
        #db_insert($db, DB_INSERT);
        db_select($db, DB_SELECT_ALIASES);
    }

    db_postinit();
}
catch
{
    die "[3]: $_";
};

# CGI.
#
# Handle those GET-parameters which we couldn't check before
# doing some database queries.
try
{
    cgi_postinit();
    cgi_progress_set($cgi);
    cgi_deinit($cgi);
}
catch
{
    die "[4]: $_";
};

# Database.
#
# View, edit, add, rate books of the viewed profile if it is
# user's profile. If it doesn't, just view.
try
{
    if (defined $db)
    {
        if    ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
        {
            db_select($db, DB_SELECT_TITLE);
        }
        elsif ($cgi_action{'final_val'} == CGI_ACTION_ADD &&
               $cgi_auth_ok &&
               $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
               $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            db_select($db, DB_SELECT_TITLES_CNT);
            db_select($db, DB_SELECT_FORMATS_CATEGORIES);
            db_insert($db, DB_INSERT_AUTHORS_PUBLISHERS_TITLE);
            db_select($db, DB_SELECT_AUTHORS_PUBLISHERS);
        }
        elsif ($cgi_action{'final_val'} == CGI_ACTION_EDIT &&
               $cgi_auth_ok &&
               $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
               $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            db_select($db, DB_SELECT_TITLE_COVER);
            db_select($db, DB_SELECT_FORMATS_CATEGORIES);
            db_insert($db, DB_INSERT_AUTHORS_PUBLISHERS_TITLE);
            db_update($db, DB_UPDATE_TITLE);
            db_delete($db);
            db_select($db, DB_SELECT_AUTHORS_PUBLISHERS);
            db_select($db, DB_SELECT_TITLE);
        }
        elsif ($cgi_action{'final_val'} == CGI_ACTION_RATE &&
               $cgi_auth_ok &&
               $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
               $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            db_update($db, DB_UPDATE_TITLE_RATING);
        }

        db_disconnect($db);
    }

    chdir "$path/../";
}
catch
{
    die "[5]: $_";
};

# HTML
if ($db_res == DB_RES_SUCCESS)
{
    # There is new or updated title. Get redirection here.
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, "/$cgi_space/$cgi_username{'verified_val'}/$db_title_id/", undef)};
}
elsif (
          # No page with such username
          !defined $db

          ||

          # Or no permission to do some action except the title viewing
          (
               !($cgi_auth_ok &&
                 $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
                 $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'}) &&
               $cgi_action{'final_val'} != CGI_ACTION_VIEW
          )

          ||

          # Or can't do SQL select for the required title
          (
              (!defined $db_title || @$db_title  == 0)
          &&
              ($cgi_action{'final_val'} == CGI_ACTION_VIEW || $cgi_action{'final_val'} == CGI_ACTION_EDIT)
          )

          ||

          # Or can't rate
          $cgi_action{'final_val'} == CGI_ACTION_RATE
      )
{
    # So 404
    print ${cgi::header_get($cgi, $cgi_seSSion, 404, undef, undef)};
}
else
{
    # Everything is OK. Show title page.
    my ($author_lbl, $title_lbl, $publisher_lbl, $isbn_lbl, $format_lbl,
            $date_lbl, $rating_lbl, $category_lbl, $progress_lbl, $comment_lbl);

    my $rows;
    my $attr_main;
    my $attr_username;

    my ($author, $title, $publisher, $isbn,
            $date, $progress_begin, $progress_end);
    my  $format;
    my  $format_i18n;
    my  $rating;
    my  $category;
    my  $category_i18n;
    my  $progress;
    my  $comment;
    my  $cover;

    my @title;

    my $forma;
    my $catorga;
    my $forma_cover;

    my $rating_row;
    my $progress_row;
    my $comment_row;

    my ($indent_table, $indent_tr, $indent_td, $indent_p);

    print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};

    if (@db_Aliases > 0)
    {
        $author_lbl    = encode_entities($db_Aliases[11], '<>&') . ":";
        $title_lbl     = encode_entities($db_Aliases[12], '<>&') . ":";
        $publisher_lbl = encode_entities($db_Aliases[13], '<>&') . ":";
        $isbn_lbl      = encode_entities($db_Aliases[14], '<>&') . ":";
        $format_lbl    = encode_entities($db_Aliases[15], '<>&') . ":";
        $date_lbl      = encode_entities($db_Aliases[16], '<>&') . ":";
        $rating_lbl    = gettext('Rating:');
        $category_lbl  = encode_entities($db_Aliases[17], '<>&') . ":";
        $progress_lbl  = encode_entities($db_Aliases[18], '<>&') . ":";
        $comment_lbl   = encode_entities($db_Aliases[19], '<>&') . ":";
    }
    else
    {
        $author_lbl    = gettext('Author:');
        $title_lbl     = gettext('Title:');
        $publisher_lbl = gettext('Publisher:');
        $isbn_lbl      = gettext('ISBN:');
        $format_lbl    = gettext('Format:');
        $date_lbl      = gettext('Date:');
        $rating_lbl    = gettext('Rating:');
        $category_lbl  = gettext('Category:');
        $progress_lbl  = gettext('Progress:');
        $comment_lbl   = gettext('Comment:');
    }

    # Get data from database.
    # Generate input data for forms in case of action is needed.
    if ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
    {
        $rows          = 9;
        $attr_main     = 'colspan="' .  db::RATING_MAX      . '" id="main_view"';
        $attr_username = 'colspan="' . (db::RATING_MAX + 1) . '" id="username"';

        ($author, $title, $publisher, $isbn,
             $date, $progress_begin, $progress_end) = map encode_entities($_,             '<>&'), @$db_title[0..3,6..6,11..12];
         $format                                    = undef;
         $format_i18n                               =     encode_entities($$db_title[ 5], '<>&');
         $rating                                    =                     $$db_title[ 7];
         $category                                  = undef;
         $category_i18n                             =     encode_entities($$db_title[ 9], '<>&');
         $progress                                  =                     $$db_title[10];
         $comment                                   =     encode_entities($$db_title[13], '<>&') ;

        push @title, "$author - $title";

        # Prepare fields for table:
        #  - format,
        $forma = $format_i18n;

        #  - rating,
        $rating_row = qq(
        <tr>
            <td id="main_hdr_lvl2">$rating_lbl</td>);

        foreach my $rating_tmp (1 .. db::RATING_MAX)
        {
            my ($id, $icon);
            my ($a_href, $a_href_);

            # <td id="...">
            if    ($rating_tmp == 1)              { $id = 'rating_left';  }
            elsif ($rating_tmp == db::RATING_MAX) { $id = 'rating_right'; }
            else                                  { $id = 'rating';       }

            # ICO picture to show
            if ($cgi_space eq 'liberation')
            {
                $icon = $rating_tmp <= $rating ? 'luna-libre.ico' :
                                                 'luna.ico';
            }
            #elsif ($cgi_space eq 'hair')
            else
            {
                $icon = $rating_tmp <= $rating ? 'cute_heart.ico' :
                                                 'cute_heart-disabled.ico';
            }

            # Allow rate links if username is logged in
            if ($cgi_auth_ok &&
                $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
                $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
            {
                $a_href  = qq(<a href="/$cgi_space/$cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}/rate/$rating_tmp/redirect/">);
                $a_href_ =  q(</a>);
            }
            else
            {
                $a_href  = '';
                $a_href_ = '';
            }

            $rating_row .= qq(
            <td id="$id">$a_href<img src="/$cgi_space/img/$icon" id="rating">$a_href_</td>);
        }

        #  - category,
        $catorga = $category_i18n;

        #  - progress,
        if ($progress == db::PROGRESS_DIS)
        {
            $progress_row = '';
        }
        else
        {
            my $td;
            my $units;

            $rows++;

            $td  = 'colspan="' . db::RATING_MAX . '" id="';
            $td .= ($progress_begin == $progress_end) ? 'main_view_liberated' :
                                                        'main_view';
            $td .= '"';

            if    ($progress == db::PROGRESS_PAGES) { $units = @db_Aliases > 0 ? encode_entities($db_Aliases[20], '<>&') : gettext('p.');   }
            #elsif ($progress == db::PROGRESS_LOC)
            else                                    { $units = @db_Aliases > 0 ? encode_entities($db_Aliases[21], '<>&') : gettext('loc.'); }

            $progress_row = qq(
        <tr>
            <td id="main_hdr_lvl2">$progress_lbl</td>
            <td $td>$progress_begin/$progress_end $units</td>);
        }

        #  - comment
        if ($comment eq '')
        {
            $comment_row = '';
        }
        else
        {
            $rows++;

            $comment_row = qq(
        <tr>
            <td id="main_hdr_lvl2">$comment_lbl</td>
            <td $attr_main>$comment</td>);
        }

        ($indent_table, $indent_tr, $indent_td, $indent_p) = (html::INDENT1, html::INDENT2, html::INDENT3, html::INDENT4);
    }
    #if ($cgi_action{'final_val'} == CGI_ACTION_ADD)
    #if ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
    #if ($cgi_action{'final_val'} == CGI_ACTION_RATE) <-- It can't be (redirect above is assumpted)
    else
    {
        my  $post_method;
        my ($pages, $locations);

        $author_lbl    = qq(<label for="$db_author_Alias" id="hdr_lvl2">$author_lbl</label>);
        $title_lbl     = qq(<label for="_$db_title_Alias" id="hdr_lvl2">$title_lbl</label>);
        $publisher_lbl = qq(<label for="$db_publisher_Alias" id="hdr_lvl2">$publisher_lbl</label>);
        $isbn_lbl      = qq(<label for="$db_isbn_Alias" id="hdr_lvl2">$isbn_lbl</label>);
        $format_lbl    = qq(<label for="$db_format_Alias" id="hdr_lvl2">$format_lbl</label>);
        $date_lbl      = qq(<label for="$db_date_Alias" id="hdr_lvl2">$date_lbl</label>);
        $category_lbl  = qq(<label for="$db_category_Alias" id="hdr_lvl2">$category_lbl</label>);
        $progress_lbl  = qq(<label for="${db_progress_Alias}_begin" id="hdr_lvl2">$progress_lbl</label>);
        $comment_lbl   = qq(<label for="$db_comment_Alias" id="hdr_lvl2">$comment_lbl</label>);

        $rows = 10;

            $post_method = $cgi->request_method() eq 'POST';
        if ($post_method)
        {
            ($author, $title, $publisher, $isbn,
                 $date, $progress_begin, $progress_end) = map encode_entities($_, '<>&'), (defined $cgi_author{'val'}         ? $cgi_author{'val'}         : '',
                                                                                           defined $cgi_title{'val'}          ? $cgi_title{'val'}          : '',
                                                                                           defined $cgi_publisher{'val'}      ? $cgi_publisher{'val'}      : '',
                                                                                           defined $cgi_isbn{'val'}           ? $cgi_isbn{'val'}           : '',
                                                                                           defined $cgi_date{'val'}           ? $cgi_date{'val'}           : '',
                                                                                           defined $cgi_progress_begin{'val'} ? $cgi_progress_begin{'val'} : '',
                                                                                           defined $cgi_progress_end{'val'}   ? $cgi_progress_end{'val'}   : '');
             $format        = defined $db_format ? $db_format : db::FORMAT_DIGITAL_EPUB;
             $format_i18n   = undef;
             $rating        = undef;
             $category      = defined $db_category ? $db_category : db::CATEGORY_COLLECTION;
             $category_i18n = undef;
             $progress      = defined $cgi_progress{'verified_val'} ? $cgi_progress{'verified_val'} : db::PROGRESS_DIS;
             $comment       = encode_entities(defined $cgi_comment{'val'} ? $cgi_comment{'val'} : '', '<>&');
             $cover         = 0;
        }

        if ($cgi_action{'final_val'} == CGI_ACTION_ADD)
        {
            $attr_main     = 'colspan="4" id="main_add"';
            $attr_username = 'colspan="5" id="username"';

            unless ($post_method)
            {
                ($author, $title, $publisher, $isbn,
                     $date, $progress_begin, $progress_end) = ('') x 7;
                 $format                                    = db::FORMAT_DIGITAL_EPUB;
                 $format_i18n                               = undef;
                 $rating                                    = undef;
                 $category                                  = db::CATEGORY_COLLECTION;
                 $category_i18n                             = undef;
                 $progress                                  = db::PROGRESS_DIS;
                 $comment                                   = '';
                 $cover                                     = 0;
            }

            push @title, gettext('Add');
        }
        #if ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
        else
        {
            $attr_main     = 'colspan="4" id="main_edit"';
            $attr_username = 'colspan="5" id="username"';

            unless ($post_method)
            {
                ($author, $title, $publisher, $isbn,
                     $date, $progress_begin, $progress_end) = map encode_entities($_,             '<>&"'), @$db_title[0..3,6..6,11..12];
                 $format                                    =                     $$db_title[ 4];
                 $format_i18n                               = undef;
                 $rating                                    = undef;
                 $category                                  =                     $$db_title[ 8];
                 $category_i18n                             = undef;
                 $progress                                  =                     $$db_title[10];
                 $comment                                   =     encode_entities($$db_title[13], '<>&');
                 $cover                                     =                     $$db_title[14];

                unless (defined $progress_begin)
                {
                    $progress_begin = '';
                    $progress_end   = '';
                }
            }

            push @title, gettext('Edit');
            push @title, "$author - $title";
        }

        $author    = qq(<input list="authors" name="$db_author_Alias" autofocus maxlength=") . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" value="$author" id="$db_author_Alias">);
        $title     = qq(<input type="text" name="$db_title_Alias" maxlength=") . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" value="$title" id="_$db_title_Alias">);
        $publisher = qq(<input list="publishers" name="$db_publisher_Alias" maxlength=") . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" value="$publisher" id="$db_publisher_Alias">);
        $isbn      = qq(<input type="text" name="$db_isbn_Alias" maxlength=") . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" value="$isbn" id="$db_isbn_Alias">);
        $date      = qq(<input type="text" name="$db_date_Alias" maxlength=") . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" value="$date" id="$db_date_Alias">);

        # Prepare fields for table:
        #  - format,
        $forma = "\n" . html::INDENT4 . qq(<select name="$db_format_Alias" id="$db_format_Alias">\n);

        foreach my $row (@$db_formats)
        {
            my ($format_val, $format_val_i18n);
            my  $sel;

            ($format_val, $format_val_i18n) = @$row;

            $sel = $format_val eq $format ? ' selected' : '';

            $format_val      = encode_entities($format_val,      '<>&"');
            $format_val_i18n = encode_entities($format_val_i18n, '<>&');

            $forma .= html::INDENT5 . qq(<option value="$format_val"$sel>$format_val_i18n\n);
        }

        $forma .= html::INDENT4 . '</select>';

        #  - rating,
        $rating_row = '';

        #  - category,
        $catorga = "\n" . html::INDENT4 . qq(<select name="$db_category_Alias" id="$db_category_Alias">\n);

        foreach my $row (@$db_categories)
        {
            my ($category_val, $category_val_i18n);
            my  $sel;

            ($category_val, $category_val_i18n) = @$row;

            $sel = $category_val eq $category ? ' selected' : '';

            $category_val      = encode_entities($category_val,      '<>&');
            $category_val_i18n = encode_entities($category_val_i18n, '<>&"');

            $catorga .= html::INDENT5 . qq(<option value="$category_val"$sel>$category_val_i18n\n);
        }

        $catorga .= html::INDENT4 . '</select>';

        #  - progress,
        if (@db_Aliases > 0)
        {
            $pages     = encode_entities($db_Aliases[20], '<>&');
            $locations = encode_entities($db_Aliases[21], '<>&');
        }
        else
        {
            $pages     = gettext('p.');
            $locations = gettext('loc.');
        }

        $progress_row = qq(
            <tr>
                <td id="main_hdr_lvl2">$progress_lbl</td>
                <td id="${db_progress_Alias}_begin"><input type="text" name="${db_progress_Alias}_begin" maxlength=") . db::PROGRESS_MAX_SIZE . qq(" value="$progress_begin" id="${db_progress_Alias}_begin"></td>
                <td id="${db_progress_Alias}_delimiter">/</td>
                <td id="${db_progress_Alias}_end"><input type="text" name="${db_progress_Alias}_end" maxlength=") . db::PROGRESS_MAX_SIZE . qq(" value="$progress_end" id="${db_progress_Alias}_end"></td>
                <td id="$db_progress_Alias">
                    <select name="$db_progress_Alias" id="$db_progress_Alias">
                        <option value=") . db::PROGRESS_DIS   . q(") . ($progress eq db::PROGRESS_DIS   ? ' selected' : '') . q(>—
                        <option value=") . db::PROGRESS_PAGES . q(") . ($progress eq db::PROGRESS_PAGES ? ' selected' : '') . qq(>$pages
                        <option value=") . db::PROGRESS_LOC   . q(") . ($progress eq db::PROGRESS_LOC   ? ' selected' : '') . qq(>$locations
                    </select>
                </td>);

        #  - comment
        $comment_row = qq(
            <tr>
                <td id="main_hdr_lvl2">$comment_lbl</td>
                <td $attr_main><textarea name="$db_comment_Alias" rows=") . db::COMMENT_MAX_LINES . '" maxlength="' . db::FIELD_MULTI_LINE_MAX_SIZE . qq(" id="$db_comment_Alias">$comment</textarea></td>);

        ($indent_table, $indent_tr, $indent_td, $indent_p) = (html::INDENT2, html::INDENT3, html::INDENT4, html::INDENT5);
    }

    push @title, $cgi_username{'verified_val'};
    push @title, $i18n_space;
    push @title, html::SITE_NAME;

    print ${html::header_get(html::title_get(@title), $cgi_space)};

    print html::INDENT1;

    if ($cgi_auth_ok &&
        $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
        $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
    {
        print qq(<span id="menu"><a href="/$cgi_space/$cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}/">), gettext('View'), q(</a></span>)          if                                                $cgi_action{'final_val'} == CGI_ACTION_EDIT;
        print qq(<span id="menu"><a href="/$cgi_space/$cgi_username{'verified_val'}/add/">), gettext('Add'), q(</a></span>)                                     if $cgi_action{'final_val'} == CGI_ACTION_VIEW || $cgi_action{'final_val'} == CGI_ACTION_EDIT;
        print qq(<span id="menu"><a href="/$cgi_space/$cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}/edit/">), gettext('Edit'), q(</a></span>)     if $cgi_action{'final_val'} == CGI_ACTION_VIEW;
        print qq(<span id="menu"><a href="/$cgi_space/$cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}/delete/">), gettext('Delete'), q(</a></span>) if $cgi_action{'final_val'} == CGI_ACTION_VIEW || $cgi_action{'final_val'} == CGI_ACTION_EDIT;
        print  q(<span id="menu_delimiter">|</span>)                                                                                                            if $cgi_action{'final_val'} == CGI_ACTION_VIEW || $cgi_action{'final_val'} == CGI_ACTION_EDIT;
    }

    print qq(<span id="menu"><a href="/$cgi_space/">), gettext('Index'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    if ($cgi_auth_ok) { print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>); }
    else              { print qq(<span id="menu"><a href="/$cgi_space/login/">), gettext('Log In'), q(</a></span>);                                                                                                                                                                                                                                                                                               }

    print << ';';


    </div>
    </div>
    <div id="main">
    <div id="centr">

;

    # Use <form> if the action should be performed (add or edit)
    # instead of its absence (just view)
    if ($cgi_action{'final_val'} != CGI_ACTION_VIEW)
    {
        print html::INDENT1, '<h1>', ($cgi_action{'final_val'} == CGI_ACTION_ADD ? gettext('Add') : gettext('Edit')), "</h1>\n";

        if ($db_res != DB_RES_NONE)
        {
            if ($db_res & DB_RES_ERR_AUTHOR)    { print html::INDENT1, '<p id="centr_err">', gettext('Author has to be non-empty'), "\n";                                                      }
            if ($db_res & DB_RES_ERR_TITLE)     { print html::INDENT1, '<p id="centr_err">', gettext('Title has to be non-empty'), "\n";                                                       }
            if ($db_res & DB_RES_ERR_PUBLISHER) { print html::INDENT1, '<p id="centr_err">', gettext('Publisher has to be non-empty'), "\n";                                                   }
            if ($db_res & DB_RES_ERR_ISBN)      { print html::INDENT1, '<p id="centr_err">', gettext('ISBN has to be non-empty'), "\n";                                                        }
            if ($db_res & DB_RES_ERR_DATE)      { print html::INDENT1, '<p id="centr_err">', gettext('Date has to be non-empty'), "\n";                                                        }
            if ($db_res & DB_RES_ERR_PROGRESS)  { print html::INDENT1, '<p id="centr_err">', gettext('Invalid progress'), "\n";                                                                }
            if ($db_res & DB_RES_ERR_COMMENT)   { print html::INDENT1, '<p id="centr_err">', gettext('Comment must be less or equal to '), db::COMMENT_MAX_LINES, gettext(' lines'), "</p>\n"; }
            if ($db_res & DB_RES_ERR_COVER_DL)  { print html::INDENT1, '<p id="centr_err">', gettext('Cover should be less than '), img::MAX_SIZE / 1024 / 1024, gettext(' MB'), "</p>\n";     }
            if ($db_res & DB_RES_ERR_COVER_BAD) { print html::INDENT1, '<p id="centr_err">', gettext('Invalid cover image'), "</p>\n";                                                         }
        }

        print << ';';
    <form method="POST" enctype="multipart/form-data" id="title">
;

    }

    # Start generating the cover HTML block.
    # Calculate timestamp for the cover.
    if ($cgi_action{'final_val'} != CGI_ACTION_ADD)
    {
        my $cover_path;
        my $stat;
        my $ts;

        $cover_path = "$cgi_space/" . img::cover::DIR . "$cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}.png";
        $stat       = stat $cover_path;
        $ts         = $stat ? '?' . $stat->mtime() :
                              '';

        $forma_cover = qq(\n$indent_p<p id="padding"><img src="/$cgi_space/) . img::cover::DIR . qq($cgi_username{'verified_val'}/$cgi_title_id{'verified_val'}.png$ts" id="cover"></p>)
    }

    # Continue generating the cover HTML block
    $forma_cover .= qq(\n$indent_p<p id="left_padding"><label><input type="checkbox" name="cover_del" value="1">) . gettext('Delete') . q(</label></p>) if $cover;
    $forma_cover .= qq(\n$indent_p<p id="padding"><input type="file" name="cover" accept="image/*"></p>)                                                if $cgi_action{'final_val'} != CGI_ACTION_VIEW;

    # Print main table or form with data
    print qq($indent_table<table id="title_rows$rows">
$indent_tr<tr>
$indent_td<th rowspan="$rows" id="cover">$forma_cover
$indent_td</th>
$indent_td<td $attr_username><span id="profile"><span id="icon"><a href="/$cgi_space/$cgi_username{'verified_val'}/profile/">💉</a></span><a href="/$cgi_space/$cgi_username{'verified_val'}/">$cgi_username{'verified_val'}</a></span></td>
$indent_tr<tr><td id="main_hdr_lvl2">$author_lbl</td><td $attr_main>$author</td>
$indent_tr<tr><td id="main_hdr_lvl2">$title_lbl</td><td $attr_main>$title</td>
$indent_tr<tr><td id="main_hdr_lvl2">$publisher_lbl</td><td $attr_main>$publisher</td>
$indent_tr<tr><td id="main_hdr_lvl2">$isbn_lbl</td><td $attr_main>$isbn</td>
$indent_tr<tr><td id="main_hdr_lvl2">$format_lbl</td><td $attr_main>$forma</td>
$indent_tr<tr><td id="main_hdr_lvl2">$date_lbl</td><td $attr_main>$date</td>$rating_row
$indent_tr<tr><td id="main_hdr_lvl2">$category_lbl</td><td $attr_main>$catorga</td>$progress_row$comment_row
$indent_table</table>\n);

    # Print additional <datalist> with authors and publishers.
    # It is conveniency to fill author and publisher input using
    # this shit.
    if ($cgi_action{'final_val'} != CGI_ACTION_VIEW)
    {
        # Print additional <datalist> with authors
        if (@$db_authors > 0)
        {
            print << ';';

        <datalist id="authors">
;

            foreach my $author (@$db_authors)
            {
                $author = encode_entities($author, '<>&"');
                print html::INDENT3, qq(<option value="$author">\n);
            }

            print << ';';
        </datalist>
;
        }

        # Print additional <datalist> with publishers
        if (@$db_publishers > 0)
        {
            print << ';';

        <datalist id="publishers">
;

            foreach my $publisher (@$db_publishers)
            {
                $publisher = encode_entities($publisher, '<>&"');
                print html::INDENT3, qq(<option value="$publisher">\n);
            }

            print << ';';
        </datalist>
;
        }

        # End of <form> if the action should be performed
        print q(
        <input type="reset" value="), gettext('Reset'), q(">
        <input type="submit" value="), $cgi_action{'final_val'} == CGI_ACTION_EDIT ? gettext('Edit') :
                                                                                     gettext('Add'), qq(">
    </form>\n);
    }

    print ${html::footer_get($cgi_space)};
}
