#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
profile.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
#use Cwd;
use DBI;
use Encode;
use File::Basename;
use File::Spec;
use File::stat;
use HTML::Entities;
use Locale::gettext;
use Regexp::Common;
use Try::Tiny;
use Unicode::String;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../lib/";
use lib "$path/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;
use img;
use img::avatar;

# Constants:
#
#  - CGI
use constant CGI_ACTION_VIEW          => 0;
use constant CGI_ACTION_EDIT          => 1;

#  - Database
use constant DB_CONNECT_AUTH          => 0;
use constant DB_CONNECT_PROFILE       => 1;

use constant DB_SELECT_LANG           => 0;
use constant DB_SELECT_PROFILE        => 1;
use constant DB_SELECT_PROFILE_AVATAR => 2;

use constant DB_RES_NONE              => 0;
use constant DB_RES_SUCCESS           => 1;
use constant DB_RES_ERR_ABOUT         => 2;
use constant DB_RES_ERR_AVATAR_DL     => 3;
use constant DB_RES_ERR_AVATAR_BAD    => 4;

# Global Variables:
#
#  - CGI,
our $cgi            = undef;
our $cgi_space      = undef;
our $cgi_seSSion    = undef;
our $cgi_auth       = undef;
our $cgi_auth_ok    = undef;

our %cgi_username   = ();
our %cgi_action     = ();

our %cgi_name       = ();
our %cgi_about      = ();
our %cgi_avatar     = ();
our %cgi_avatar_del = ();

#  - Database,
our $db             = undef;

our $db_locale      = undef;

our $db_name        = undef;
our $db_about       = undef;
our $db_avatar      = undef;

our $db_res         = DB_RES_NONE;

#  - i18n
our $i18n_space     = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    my $cgi;

    $cgi       = cgi::init();
    $cgi_space = cgi::space_get();

    $cgi_username  {'val'         } = $cgi->url_param('username');
    $cgi_username  {'verified_val'} = defined $cgi_username{'val'} && Unicode::String::utf8($cgi_username{'val'})->length() <= db::USERNAME_MAX_SIZE &&
                                                                      $cgi_username{'val'} =~ /^[a-z0-9_]+\z/ ?
                                          $cgi_username{'val'} : undef;

    $cgi_action    {'val'         } = $cgi->url_param('action');
    $cgi_action    {'verified_val'} = defined $cgi_action{'val'} && $cgi_action{'val'} eq 'edit' ?
                                          $cgi_action{'val'} : undef;

    $cgi_name      {'val'         } = $cgi->param('name');
    $cgi_name      {'verified_val'} = defined $cgi_name{'val'} && Unicode::String::utf8($cgi_name{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                  $cgi_name{'val'} !~ /\n|\r/ ?
                                          $cgi_name{'val'} : undef;

    $cgi_about     {'val'         } = $cgi->param('about');
    $cgi_about     {'verified_val'} = defined $cgi_about{'val'} && Unicode::String::utf8($cgi_about{'val'})->length() < db::FIELD_MULTI_LINE_MAX_SIZE &&
                                                                   (() = $cgi_about{'val'} =~ /\n/g) <= db::ABOUT_MAX_LINES - 1 &&
                                                                   (() = $cgi_about{'val'} =~ /\r/g) <= db::ABOUT_MAX_LINES - 1 ?
                                          $cgi_about{'val'} : undef;

    $cgi_avatar    {'val'         } = $cgi->param('avatar');
    $cgi_avatar    {'verified_val'} = defined $cgi_avatar{'val'} && $cgi_avatar{'val'} ne '' ?
                                          $cgi_avatar{'val'} : undef;

    $cgi_avatar_del{'val'         } = $cgi->param('avatar_del');
    $cgi_avatar_del{'verified_val'} = defined $cgi_avatar_del{'val'} && $cgi_avatar_del{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_avatar_del{'val'} == 1 ?
                                          $cgi_avatar_del{'val'} : undef;

    return $cgi;
}

################################################################################

sub cgi_action_set
{
    my $cgi = shift;

    $cgi_action{'final_val'} = defined $cgi_action{'verified_val'} ? CGI_ACTION_EDIT :
                                                                     CGI_ACTION_VIEW;
}

################################################################################

sub cgi_username_set
{
    my $cgi = shift;

    $cgi_username{'final_val'} = $cgi_action{'final_val'} == CGI_ACTION_VIEW ? $cgi_username{'verified_val'} :
                                                                               $cgi_auth->{'profile'}{'username'},
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    my $username;
    my $db;

    my $action = shift;

    if ($action == DB_CONNECT_AUTH)
    {
        $username = $cgi_auth->{'profile'}{'username'};
    }
    #elsif ($action == DB_CONNECT_PROFILE)
    else
    {
        $username = $cgi_username{'final_val'};
    }

    $db = db::connect($username,
                      0 # Do not create database if it doesn't exist
                     );
    img::avatar::init($username) if defined $db;

    return $db;
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_select
{
    my $db     = shift;
    my $action = shift;

    if ($action == DB_SELECT_LANG)
    {
        $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
    }
    elsif ($action == DB_SELECT_PROFILE)
    {
        return if defined $cgi_name      {'val'} ||
                  defined $cgi_about     {'val'} ||
                  defined $cgi_avatar    {'val'} ||
                  defined $cgi_avatar_del{'val'};

    ($db_name, $db_about, $db_avatar) = $db->selectrow_array(<< ';');
SELECT name, about, avatar
FROM   profile
;
    }
    #elsif ($action == DB_SELECT_PROFILE_AVATAR)
    else
    {
        return unless defined $cgi_name      {'val'} ||
                      defined $cgi_about     {'val'} ||
                      defined $cgi_avatar    {'val'} ||
                      defined $cgi_avatar_del{'val'};

        return unless defined $cgi_name{'verified_val'};

        unless (defined $cgi_about{'verified_val'})
        {
            $db_res = DB_RES_ERR_ABOUT;
            return;
        }

        return if defined $cgi_avatar{'verified_val'} && defined $cgi_avatar_del{'verified_val'};

        $db_res    = DB_RES_SUCCESS;
        $db_avatar = ($db->selectrow_array(<< ';'))[0];
SELECT avatar
FROM   profile
;
    }
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_update
{
    my $tmp;
    my $img;
    my $query;

    my $db = shift;

    return unless defined $db_avatar;

    if (defined $cgi_avatar{'verified_val'})
    {
                        $tmp = img::avatar::dl($cgi_avatar{'verified_val'});
        unless (defined $tmp)
        {
            $db_res = DB_RES_ERR_AVATAR_DL;
            return;
        }
    }

    if (defined $cgi_avatar{'verified_val'} || defined $cgi_avatar_del{'verified_val'} || $db_avatar == 0)
    {
        img::avatar::dir_change();

        if (!img::avatar::make(\$img,

                               \$tmp,

                               \$cgi_name{'verified_val'},
                               \''))
        {
            $db_res = DB_RES_ERR_AVATAR_BAD;

            img::avatar::dir_back_change();
            img::avatar::dl_complete();
            return;
        }

        img::avatar::dl_complete() if defined $cgi_avatar{'verified_val'};
        img::avatar::make_complete($img, \$cgi_username{'final_val'});
        img::avatar::dir_back_change();
    }

    db::profile_update($db,

                       \$cgi_name{'verified_val'},
                       \$cgi_about{'verified_val'},
                       defined $cgi_avatar{'verified_val'} ||
                           (!defined $cgi_avatar_del{'verified_val'} && $db_avatar == 1) ? 1 : 0);


    $db_res = DB_RES_SUCCESS;
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# i18n
################################################################################

sub i18n_space_set
{
    Encode::_utf8_on($cgi_space);
    $i18n_space = gettext(ucfirst $cgi_space);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init        ();
                                cgi_action_set  ($cgi);
    ($cgi_seSSion, $cgi_auth) = cgi_auth        ($cgi);
                                cgi_username_set($cgi);
                                cgi_deinit      ($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/../$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect(DB_CONNECT_AUTH);

        if (defined $db)
        {
            #db_create($db);
            #db_insert($db);
            db_select($db, DB_SELECT_LANG);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir "$path/../";

    i18n::init(\$db_locale, $cgi);
    i18n_space_set();
}
catch
{
    die "[1]: $_";
};

# Database. View and update user's profile.
try
{
    chdir $path;

    $db = db_connect(DB_CONNECT_PROFILE);

    if (defined $db)
    {
        #db_create($db);
        #db_insert($db);

        if    ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
        {
            db_select($db, DB_SELECT_PROFILE);
        }
        elsif (#$cgi_action{'final_val'} == CGI_ACTION_EDIT &&
               $cgi_auth_ok)
        {
            db_select($db, DB_SELECT_PROFILE_AVATAR);
            db_update($db);
            db_select($db, DB_SELECT_PROFILE);
        }

        db_disconnect($db);
    }

    chdir "$path/../";
}
catch
{
    die "[2]: $_";
};

# HTML
if ($db_res == DB_RES_SUCCESS)
{
    # The profile is updated. Get redirection here.
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, "/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/", undef)};
}
elsif (
          # No page with such username
          !defined $db

          ||

          # Or can't do SQL select for the required profile
          (
              (defined $cgi_name      {'val'} ||
               defined $cgi_about     {'val'} ||
               defined $cgi_avatar    {'val'} ||
               defined $cgi_avatar_del{'val'})
          &&
              $cgi_action{'final_val'} == CGI_ACTION_VIEW
          )

          ||

          # Or no permission to do some action except the title viewing
          (
              !$cgi_auth_ok &&
              $cgi_action{'final_val'} == CGI_ACTION_EDIT
          )
      )
{
    # So 404
    print ${cgi::header_get($cgi, $cgi_seSSion, 404, undef, undef)};
}
else
{
    # Everything is OK. Show title page.
    my ($name_lbl, $about_lbl);
    my $attr;
    my ($name, $about);
    my $avatar;
    my @title;
    my $avatar_path;
    my $stat;
    my $ts;
    my $forma_avatar;
    my ($indent_table, $indent_tr, $indent_td, $indent_p);

    print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};

    $name_lbl  = gettext('Name:');
    $about_lbl = gettext('About:');

    # Get data from database.
    # Generate input data for forms in case of action is needed.
    if ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
    {
        push @title, gettext('Profile');
        push @title, $cgi_username{'verified_val'};

        $attr = 'id="main_view"';

        $name   = encode_entities($db_name,  '<>&');
        $about  = encode_entities($db_about, '<>&');
        $avatar = undef;

        ($indent_table, $indent_tr, $indent_td, $indent_p) = (html::INDENT1, html::INDENT2, html::INDENT3, html::INDENT4);
    }
    #if ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
    else
    {
        push @title, gettext('Edit Profile');

        $attr = 'id="main_edit"';

        $name_lbl  = qq(<label for="name" id="hdr_lvl2">$name_lbl</label>);
        $about_lbl = qq(<label for="about" id="hdr_lvl2">$about_lbl</label>);

        $name   = ' value="' . encode_entities($db_name,  '<>&') . '"';
        $about  =              encode_entities($db_about, '<>&"');
        $avatar = $db_avatar;

        $name  = '<input type="text" name="name" autofocus maxlength="' . db::FIELD_SINGLE_LINE_MAX_SIZE . qq("$name id="name">);
        $about = '<textarea name="about" rows="' . db::ABOUT_MAX_LINES . '" maxlength="' . db::FIELD_MULTI_LINE_MAX_SIZE . qq(">$about</textarea>);

        ($indent_table, $indent_tr, $indent_td, $indent_p) = (html::INDENT2, html::INDENT3, html::INDENT4, html::INDENT5);
    }

    push @title, $i18n_space;
    push @title, html::SITE_NAME;

    print ${html::header_get(html::title_get(@title), $cgi_space)};

    print html::INDENT1;

    if ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
    {
        if ($cgi_auth_ok &&
            $cgi_space                    eq $cgi_auth->{'profile'}{'space'} &&
            $cgi_username{'verified_val'} eq $cgi_auth->{'profile'}{'username'})
        {
            print qq(<span id="menu"><a href="/$cgi_space/profile/">), gettext('Edit'), q(</a></span>);
            print q(<span id="menu_delimiter">|</span>);
        }
    }
    #elsif ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
    else
    {
        print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">), gettext('View'), q(</a></span>);
        print  q(<span id="menu_delimiter">|</span>);
    }

    print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    if ($cgi_auth_ok) { print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>); }
    else              { print qq(<span id="menu"><a href="/$cgi_space/login/">), gettext('Log In'), q(</a></span>);                                                                                                                                                                                                                                                                                               }

    print << ';';


    </div>
    </div>
    <div id="main">
    <div id="centr">

;

    # Use <form> if the action should be performed (edit)
    # instead of its absence (just view)
    if ($cgi_action{'final_val'} == CGI_ACTION_VIEW)
    {
        print html::INDENT1, '<h1>', gettext('Profile'), "</h1>\n";
    }
    #elsif ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
    else
    {
        print html::INDENT1, '<h1>', gettext('Edit Profile'), "</h1>\n";

        if ($db_res != DB_RES_NONE)
        {
                                                       print html::INDENT1;
            if    ($db_res == DB_RES_ERR_ABOUT)      { print '<p id="centr_err">', gettext('About must be less or equal to '), db::ABOUT_MAX_LINES, gettext(' lines'), '</p>';  }
            elsif ($db_res == DB_RES_ERR_AVATAR_DL)  { print '<p id="centr_err">', gettext('Avatar should be less than '), img::MAX_SIZE / 1024 / 1024, gettext(' MB'), '</p>'; }
            #elsif ($db_res == DB_RES_ERR_AVATAR_BAD)
            else                                     { print '<p id="centr_err">', gettext('Invalid avatar image'), '</p>';                                                     }
                                                       print "\n";
        }

        print << ';';
    <form method="POST" enctype="multipart/form-data" id="profile">
;
    }

    # Start generating the avatar HTML block.
    # Calculate timestamp for the avatar.
    $avatar_path = "$cgi_space/" . img::avatar::DIR . "$cgi_username{'final_val'}.png";
    $stat        = stat $avatar_path;
    $ts          = ($stat) ? '?' . $stat->mtime() :
                             '';

    # Continue generating the avatar HTML block
    $forma_avatar  = qq(\n$indent_p<p id="padding"><img src="/$cgi_space/) . img::avatar::DIR . qq($cgi_username{'final_val'}.png$ts" id="avatar"></p>);
    $forma_avatar .= qq(\n$indent_p<p id="left_padding"><label><input type="checkbox" name="avatar_del" value="1">) . gettext('Delete') . q(</label></p>) if $avatar;
    $forma_avatar .= qq(\n$indent_p<p id="padding"><input type="file" name="avatar" accept="image/*"></p>)                                                if $cgi_action{'final_val'} == CGI_ACTION_EDIT;

    # Print main table or form with data
    print qq($indent_table<table id="profile">
$indent_tr<tr>
$indent_td<th rowspan="3" id="avatar">$forma_avatar
$indent_td</th>
$indent_td<td colspan="2" id="username"><span id="profile"><span id="icon"><a href="/$cgi_space/$cgi_username{'final_val'}/profile/">💉</a></span><a href="/$cgi_space/$cgi_username{'final_val'}/">$cgi_username{'final_val'}</a></span></td>
$indent_tr<tr><td id="main_hdr_lvl2">$name_lbl</td><td $attr>$name</td>
$indent_tr<tr><td id="main_hdr_lvl2">$about_lbl</td><td $attr>$about</td>
$indent_table</table>\n);

    # End of <form> if the action should be performed
    if ($cgi_action{'final_val'} == CGI_ACTION_EDIT)
    {
        print q(
        <input type="reset" value="), gettext('Reset'), q(">
        <input type="submit" value="), gettext('Edit'), qq(">
    </form>\n);
    }

    print ${html::footer_get($cgi_space)};
}
