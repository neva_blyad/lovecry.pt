#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
auth.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
use CGI;
use Crypt::OpenPGP;
#use Cwd;
use Encode;
use File::Basename;
use File::Spec;
use Locale::gettext;
use Regexp::Common;
use String::Random;
use Try::Tiny;
use Unicode::String;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/../lib/";
use lib "$path/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;

# Constants:
#
#  - CGI,
use constant CGI_AUTH_STAGE0    => 0;
use constant CGI_AUTH_STAGE1    => 1;
use constant CGI_AUTH_STAGE2    => 2;
use constant CGI_AUTH_STAGE3    => 3;

#  - Database,
use constant DB_SELECT_PASSWORD => 0;
use constant DB_SELECT_LANG     => 1;

#  - PGP
use constant PASSPHRASE_SIZE    => 18;

# Global Variables:
#
#  - CGI,
our $cgi              = undef;
our $cgi_space        = undef;
our $cgi_seSSion      = undef;
our $cgi_auth         = undef;
our $cgi_auth_ok      = undef;

our %cgi_action       = ();

our %cgi_log_username = ();
our %cgi_log_password = ();

our %cgi_stage        = ();

our %cgi_passphrase   = ();

#  - Database,
our $db_password      = undef;
our $db_locale        = undef;
our $db_pgp           = undef;

#  - i18n
our $i18n             = undef;
our $i18n_space       = undef;

#  - PGP,
our $pgp_passphrase   = undef;
our $pgp_ciphertext   = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    my $cgi;

    $cgi       = cgi::init();
    $cgi_space = cgi::space_get();

    $cgi_action      {'val'         } = $cgi->url_param('action');
    $cgi_action      {'verified_val'} = defined $cgi_action{'val'} && $cgi_action{'val'} eq 'logout' ?
                                            $cgi_action{'val'} : undef;

    $cgi_log_username{'val'         } = $cgi->param('log_username');
    $cgi_log_username{'verified_val'} = defined $cgi_log_username{'val'} && Unicode::String::utf8($cgi_log_username{'val'})->length() <= db::USERNAME_MAX_SIZE &&
                                                                            $cgi_log_username{'val'} =~ /^[a-z0-9_]+\z/ ?
                                            $cgi_log_username{'val'} : undef;

    $cgi_log_password{'val'         } = $cgi->param('log_password');
    $cgi_log_password{'verified_val'} = defined $cgi_log_password{'val'} && $cgi_log_password{'val'} ne '' &&
                                                                            Unicode::String::utf8($cgi_log_password{'val'})->length() <= db::PASSWORD_MAX_SIZE ?
                                            $cgi_log_password{'val'} : undef;

    $cgi_stage       {'val'         } = $cgi->param('stage');
    $cgi_stage       {'verified_val'} = defined $cgi_stage{'val'} && $cgi_stage{'val'} =~ /^$RE{'num'}{'int'}\z/ && $cgi_stage{'val'} >= CGI_AUTH_STAGE0 &&
                                                                                                                    $cgi_stage{'val'} <= CGI_AUTH_STAGE3 ?
                                            $cgi_stage{'val'} : undef;

    $cgi_passphrase  {'val'         } = $cgi->param('passphrase');
    $cgi_passphrase  {'verified_val'} = defined $cgi_passphrase{'val'} && $cgi_passphrase{'val'} ne '' &&
                                                                          Unicode::String::utf8($cgi_passphrase{'val'})->length() <= db::FIELD_SINGLE_LINE_MAX_SIZE &&
                                                                          $cgi_passphrase{'val'} !~ /\n|\r/ ?
                                            $cgi_passphrase{'val'} : undef;

    return $cgi;
}

################################################################################

sub cgi_stage_set
{
    $cgi_stage{'final_val'} = defined $cgi_stage{'val'} ? $cgi_stage{'verified_val'} :
                                                          CGI_AUTH_STAGE0;
}

################################################################################

sub cgi_auth
{
    my $action;

    my $seSSion;
    my $auth;

    my $cgi = shift;

    $action = cgi::AUTH_USE_SESSION;

    if (defined $cgi_action{'verified_val'})
    {
        $action = cgi::AUTH_LOGOUT unless defined $cgi_log_username{'verified_val'} ||
                                          defined $cgi_log_password{'verified_val'} ||

                                          !defined $cgi_stage{'final_val'} ||
                                                   $cgi_stage{'final_val'} != CGI_AUTH_STAGE0;
    }
    else
    {
        $action = cgi::AUTH_LOGIN  if     defined $cgi_log_username{'verified_val'} &&
                                          defined $cgi_log_password{'verified_val'} &&

                                          defined $cgi_stage{'final_val'} &&
                                                  $cgi_stage{'final_val'} == CGI_AUTH_STAGE3;
    }

    ($seSSion, $auth) = cgi::auth($cgi, $action, $db_password,
                                  $cgi_stage{'final_val'} == CGI_AUTH_STAGE2 ? $pgp_passphrase : undef,
                                  $cgi_space);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    my $username;

    if ($cgi_auth_ok)
    {
        $username = $cgi_auth->{'profile'}{'username'};
    }
    else
    {
        return undef unless defined $cgi_log_username{'verified_val'} &&
                            defined $cgi_log_password{'verified_val'};

        $username = $cgi_log_username{'verified_val'};
    }

    return db::connect($username,
                       0 # Do not create database if it doesn't exist
                      );
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_select
{
    my $db     = shift;
    my $action = shift;

    if ($action == DB_SELECT_PASSWORD)
    {
        ($db_password, $db_pgp) = $db->selectrow_array(<< ';');
SELECT password, pgp
FROM   settings
;
    }
    #elsif ($action == DB_SELECT_LANG)
    else
    {
        $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
    }
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# i18n
################################################################################

sub i18n_init
{
    i18n::init(\$db_locale, $cgi);

    $i18n = i18n::get();
    $i18n = $i18n eq 'C' ? '' :
                           '_' . substr $i18n, 0, 2;
}

################################################################################

sub i18n_space_set
{
    Encode::_utf8_on($cgi_space);
    $i18n_space = gettext(ucfirst $cgi_space);
}

################################################################################
# Main
################################################################################

# CGI
try
{
    $cgi = cgi_init();
           cgi_stage_set($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get password from the user's DB, whose wants to login.
try
{
    my $db;

    chdir $path;

    $db = db_connect();

    if (defined $db)
    {
        #db_create($db);
        #db_insert($db);
        db_select($db, DB_SELECT_PASSWORD);
        db_disconnect($db);
    }
}
catch
{
    die "[1]: $_";
};

# PGP
try
{
    if ($cgi_stage{'final_val'} == CGI_AUTH_STAGE0)
    {
        ;
    }
    elsif ($cgi_stage{'final_val'} == CGI_AUTH_STAGE1)
    {
        if (defined $db_pgp)
        {
            if ($db_pgp eq '')
            {
                $cgi_stage{'final_val'} = CGI_AUTH_STAGE3;
            }
            else
            {
                my $passphrase;
                my $key;
                my $cipher;

                # Generate random string — passphrase
                $pgp_passphrase = String::Random::random_string('.' x PASSPHRASE_SIZE);

                # Encrypt it with user's public PGP key
                $key    = Crypt::OpenPGP::KeyRing->new('Data' => $db_pgp);
                $cipher = Crypt::OpenPGP->new('PubRing' => $key,
                                              'Armour'  => 1);

                $pgp_ciphertext = $cipher->encrypt('Compat'     => 'GnuPG',
                                                   'Data'       => $pgp_passphrase,
                                                   'Recipients' => '.');

                $cgi_stage{'final_val'} = CGI_AUTH_STAGE2;
            }
        }
        else
        {
            $cgi_stage{'final_val'} = CGI_AUTH_STAGE0;
        }
    }
    elsif ($cgi_stage{'final_val'} == CGI_AUTH_STAGE2)
    {
        $cgi_stage{'final_val'} = defined $db_pgp ? CGI_AUTH_STAGE3 : 
                                                    CGI_AUTH_STAGE0;
    }
    #elsif ($cgi_stage{'final_val'} == CGI_AUTH_STAGE3)
    else
    {
        $cgi_stage{'final_val'} = CGI_AUTH_STAGE0;
    }
}
catch
{
    die "[2]: $_";
};

# CGI
try
{
    ($cgi_seSSion, $cgi_auth) = cgi_auth  ($cgi);
                                cgi_deinit($cgi);
}
catch
{
    die "[3]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/../$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect();
              db_select($db, DB_SELECT_LANG);
              db_disconnect($db);
    }
}
catch
{
    die "[4]: $_";
};

# i18n & l10n
try
{
    chdir "$path/../";

    i18n_init();
    i18n_space_set();
}
catch
{
    die "[5]: $_";
};

# HTML
if ($cgi_stage{'final_val'} == CGI_AUTH_STAGE3 &&
    defined $cgi_auth && $cgi_auth->{'changed'})
{
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, $cgi_auth->loggedIn() ? "/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/" :
                                                                               '/liberation/neva_blyad/', undef)};
}
else
{
    print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};
    print ${html::header_get(html::title_get(gettext('Authentication'),
                                             $i18n_space,
                                             html::SITE_NAME), $cgi_space)};

    print html::INDENT1;

    print qq(<span id="menu"><a href="/$cgi_space/">), gettext('Index'), q(</a></span>);
    print q(<span id="menu_delimiter">|</span>);

    print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);

    if ($cgi_auth_ok)
    {
        print q(<span id="menu_delimiter">|</span>);

        print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>);
    }

    print qq(

    </div>
    </div>
    <div id="main">
    <div id="centr">

    <div id="centr_full">
        <form action="/$cgi_space/login/" method="POST" id="auth">
            <h1>$i18n_space</h1>);

    if ($cgi_stage{'final_val'} == CGI_AUTH_STAGE0 ||
        $cgi_stage{'final_val'} == CGI_AUTH_STAGE3)
    {
        my $username;

        $username = $cgi_space eq 'liberation' ? gettext('The Liberated:') : gettext('Minimarket:');
        print qq(
            <table id="auth">
                <tr><td><label for="log_username">$username</label></td><td><input type="text" name="log_username" autofocus maxlength=") . db::USERNAME_MAX_SIZE . '" id="log_username"></td>
                <tr><td><label for="log_password">', gettext('Password:'), '</label></td><td><input type="password" name="log_password" maxlength="' . db::PASSWORD_MAX_SIZE . '" id="log_password"></td>
            </table>

            <input type="hidden" name="stage" value="' . CGI_AUTH_STAGE1 . '">';
    }
    #elsif ($cgi_stage{'final_val'} == CGI_AUTH_STAGE1)
    else
    {
        print '
            <table id="auth">
                <tr><td colspan="2">' . gettext('The passphrase is encrypted with your public PGP key.') . '<br>' . gettext('Below is ciphertext:') . qq(</td>
                <tr><td colspan="2"><textarea rows="14" readonly>$pgp_ciphertext</textarea></td>
                <tr><td>) . gettext('Decrypt it:') . '<td><input type="password" name="passphrase" maxlength="' . db::FIELD_SINGLE_LINE_MAX_SIZE . qq(" id="log_password"></td>
            </table>

            <input type="hidden" name="log_username" value="$cgi_log_username{'verified_val'}">
            <input type="hidden" name="log_password" value="$cgi_log_password{'verified_val'}">

            <input type="hidden" name="stage" value=") . CGI_AUTH_STAGE2 . '">';
    }

    print '
            <input type="submit" value="' . gettext('Log In') . '">
        </form>
    </div>';

    print ${html::footer_get($cgi_space)};
}
