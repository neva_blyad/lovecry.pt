#!/usr/bin/env perl

=pod

=encoding UTF-8

=head1 NAME

LoveCry.pt
index.pl

=head1 VERSION

0.04

=head1 DESCRIPTION

Liberation Web Engine

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                        <neva_blyad@lovecri.es>

This file is part of LoveCry.pt.

LoveCry.pt is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

LoveCry.pt is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.

To see a copy of the license and information with full list of Non-AGPL
components distributed by us please visit:
www.lovecry.pt/copying/

=head1 AUTHORS

=over

=item НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                      <neva_blyad@lovecri.es>

=item Invisible Light

=back

=cut

# We need to stick with this old shit by now, sry
use v5.8.9;

# We are serious about Perl
use strict;
use warnings;

# Some I/O functions should fuck up everything
use autodie 'chdir';

# CPAN modules
#use Cwd;
use File::Basename;
use File::Spec;
use Locale::gettext;
use Try::Tiny;

# Path to binary
our $path;

# Let's take it so there will be no problems with module path
# determenation below
BEGIN
{
    #$path = Cwd::abs_path(__FILE__); # This subroutine resolves symbolic links; it's bad for us
    $path = File::Spec->rel2abs(__FILE__);
    $path = File::Basename::dirname($path);
}

# Module path
use lib "$path/lib/";
use lib "$path/liberation/lib/";

# Our own modules
use cgi;
use db;
use html;
use i18n;

# Global Variables:
#
#  - CGI,
our $cgi         = undef;
our $cgi_seSSion = undef;
our $cgi_auth    = undef;
our $cgi_auth_ok = undef;

#  - Database
our $db_locale   = undef;

################################################################################
# CGI
################################################################################

sub cgi_init
{
    return cgi::init();
}

################################################################################

sub cgi_auth
{
    my $seSSion;
    my $auth;

    my $cgi = shift;

    ($seSSion, $auth) = cgi::auth($cgi, cgi::AUTH_USE_SESSION, undef, undef, undef);
    $cgi_auth_ok = defined $auth && $auth->loggedIn();

    return ($seSSion, $auth);
}

################################################################################

sub cgi_deinit
{
    my $cgi = shift;

    cgi::deinit($cgi);
}

################################################################################
# Database
################################################################################

sub db_connect
{
    my $username = shift;

    return db::connect($username,
                       0 # Do not create database if it doesn't exist
                      );
}

################################################################################

sub db_create
{
    my $db = shift;

    db::create($db);
}

################################################################################

sub db_insert
{
    my $db = shift;

    db::insert($db);
}

################################################################################

sub db_select
{
    my $db     = shift;
    my $action = shift;

    $db_locale = $i18n::locale{($db->selectrow_array(<< ';'))[0]}[0];
SELECT lang
FROM   settings
;
}

################################################################################

sub db_disconnect
{
    my $db = shift;

    $db->commit();

    db::disconnect($db);
}

################################################################################
# Main
################################################################################

# CGI
try
{
     $cgi                     = cgi_init  ();
    ($cgi_seSSion, $cgi_auth) = cgi_auth  ($cgi);
                                cgi_deinit($cgi);
}
catch
{
    die "[0]: $_";
};

# Database.
#
# Get locale settings from the current user's DB if he is logged.
try
{
    if ($cgi_auth_ok)
    {
        my $db;

        chdir "$path/$cgi_auth->{'profile'}{'space'}/";

        $db = db_connect($cgi_auth->{'profile'}{'username'});

        if (defined $db)
        {
            #db_create    ($db);
            #db_insert    ($db);
            db_select    ($db);
            db_disconnect($db);
        }
    }
}
catch
{
    die "[1]: $_";
};

# i18n & l10n
try
{
    chdir $path;

    i18n::init(\$db_locale, $cgi);
}
catch
{
    die "[2]: $_";
};

# HTML
print ${cgi::header_get($cgi, $cgi_seSSion, undef, undef, undef)};
print ${html::header_get(html::title_get(html::SITE_NAME))};

print html::INDENT1;

print qq(<span id="menu"><a href="/settings/">), gettext('Settings'), q(</a></span>);

if ($cgi_auth_ok)
{
    print q(<span id="menu_delimiter">|</span>);

    print qq(<span id="menu"><a href="/$cgi_auth->{'profile'}{'space'}/logout/">), gettext('Log Out'), qq(</a> (<span id="profile"><span id="icon"><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/profile/">💉</a></span><a href="/$cgi_auth->{'profile'}{'space'}/$cgi_auth->{'profile'}{'username'}/">$cgi_auth->{'profile'}{'username'}</a></span>)</span>);
}

print << ';';


    </div>
    </div>
    <div id="main">
    <div id="centr">
;

print '
    <div id="centr_full">
        <div id="tile">
            <p><a href="/liberation/"><img src="/liberation/img/luna-libre_grey.png"></a></p>
            <p><h1><a href="/liberation/">', gettext('Liberation'), '</a></h1></p>
        </div>
        <div id="tile">
            <p><a href="/hair/"><img src="/hair/img/cute_heart.png"></a></p>
            <p><h1><a href="/hair/">', gettext('Hair'), '</a></h1></p>
        </div>
    </div>', "\n";

print ${html::footer_get()};
