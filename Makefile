#    Makefile
#    Copyright (C) 2019-2022 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
#                                            <neva_blyad@lovecri.es>
#    
#    This file is part of LoveCry.pt.
#    
#    LoveCry.pt is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#    
#    LoveCry.pt is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU Affero General Public License
#    along with LoveCry.pt.  If not, see <https://www.gnu.org/licenses/>.
#    
#    To see a copy of the license and information with full list of Non-AGPL
#    components distributed by us please visit:
#    www.lovecry.pt/copying/

.PHONY: all pot po_init po_upd mo clean

VER := 0.04

PERL_FILES = $(shell find ./ -name '*.pl' -or \
                             -name '*.pm')

# Alias for the generating MO files
all: mo

# Generate POT file
pot:
	xgettext --output='locale/messages.pot' \
	         --language=Perl                \
	         --from-code=UTF-8              \
	         --add-comments                 \
	         --sort-by-file                 \
	         --package-name=lovecry.pt      \
	         --package-version=$(VER)       \
	                                        \
	         $(PERL_FILES)

# Generate PO files.
# The target silently overwrites existing files, which edited manually by
# translators, that's why I commented out this code.
# Uncomment only for generating new language PO for the first time.
#po_init: pot
#	msginit --input='locale/messages.pot'                        \
#	        --output-file='locale/ru_RU/LC_MESSAGES/messages.po' \
#	        --locale=ru_RU.UTF-8

# Update PO files
po_upd: pot
	msgmerge --update                               \
	         --backup=off                           \
	         --sort-by-file                         \
	                                                \
	         'locale/ru_RU/LC_MESSAGES/messages.po' \
	         'locale/messages.pot'

# Generate MO files
mo: po_upd
	msgfmt --output-file='locale/ru_RU/LC_MESSAGES/messages.mo' \
	                                                            \
	       'locale/ru_RU/LC_MESSAGES/messages.po'

# Delete POT file.
# Delete MO files.
clean:
	rm -f 'locale/messages.pot'
	rm -f 'locale/**/LC_MESSAGES/messages.mo'
