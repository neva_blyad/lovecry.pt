# NAME

LoveCry.pt

# VERSION

0.04

# DESCRIPTION

Liberation Web Engine

# MASCOT

![Alt Text](liberation/img/luna-libre.png "LoveCry.pt mascot")

**Luna batterfly** is a project mascot.

whereideasoverlap is designer of original 16x16 icon.

# SCREENSHOTS

![Alt Text](screenshot1.png "LoveCry.pt screenshot 1")
![Alt Text](screenshot2.png "LoveCry.pt screenshot 2")

# COPYRIGHT AND LICENSE

    Copyright (C) 2019-2023 НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                                            <neva_blyad@lovecri.es>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

    To see a copy of the license and information with full list of Non-AGPL
    components distributed by us please visit:
    www.lovecry.pt/copying/

# AUTHORS

    НЕВСКИЙ БЛЯДИНА <neva_blyad@lovecry.pt>
                    <neva_blyad@lovecri.es>
    Invisible Light
